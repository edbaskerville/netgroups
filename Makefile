ifneq ("$(wildcard /usr/bin/clang)","")
	CPP = clang++
	EXTRA_FLAGS = -stdlib=libc++
else
	CPP = g++
	EXTRA_FLAGS = 
endif

.PHONY: exec
exec:
	mkdir -p bin
	$(CPP) -O3 -std=c++11 $(EXTRA_FLAGS) -I$(HOME)/include -Idagoberto/src -Izppdata/src -Izppsim/src -lsqlite3 dagoberto/src/*.cpp zppdata/src/*.cpp zppsim/src/*.cpp src/*.cpp -o bin/gnmodel

.PHONY: clean
clean:
	rm -Rf bin
