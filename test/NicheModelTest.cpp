#include "catch.hpp"
#include "NicheModel.hpp"
#include "MCMC.hpp"

#include <vector>
#include <iostream>
#include <random>
#include "netgroups_util.hpp"

using namespace std;
using namespace Eigen;
using namespace zppsim;

TEST_CASE("niche-model likelihood", "[NicheModel]")
{
	random_device rd;
	bernoulli_distribution bd(0.1);
	
	double const tol = 1e-3;
	
	int64_t nSpecies = 10;
	MatrixXi64 data(nSpecies, nSpecies);
	for(int64_t i = 0; i < nSpecies; i++) {
		for(int64_t j = 0; j < nSpecies; j++) {
			data(i,j) = bd(rd);
		}
	}
	
	cerr << data << endl;
	
	NicheModel model(&data);
	
	double nicheAlpha = model.nicheAlphaNode();
	
	Vector3d pAlpha = model.pAlphaNode();
	Vector3d pBeta = model.pBetaNode();
	
	double logP = 0.0;
	logP += exponentialLogP(1.0, nicheAlpha);
	logP += exponentialLogPVector(1.0, pAlpha);
	logP += exponentialLogPVector(1.0, pBeta);
	for(int64_t pred = 0; pred < nSpecies; pred++) {
		double logPOnePred = -INFINITY;
		for(int64_t start = 0; start < nSpecies; start++) {
			for(int64_t end = start; end <= nSpecies; end++) {
				double logPOneNiche = 0.0;
				
				// Conditional probability of niche location (Dirichlet-multinomial)
				int64_t n[3] = {start, end - start, nSpecies - end};
				assert(n[0] + n[1] + n[2] == nSpecies);
				logPOneNiche += log(nSpecies) + lbeta(3.0 * nicheAlpha, nSpecies);
				for(int64_t i = 0; i < 3; i++) {
					if(n[i] > 0) {
						logPOneNiche -= log(n[i]) + lbeta(nicheAlpha, n[i]);
					}
				}
				
				// Probability of links below, within, and above niche
				int64_t sums[3];
				sums[0] = data.col(pred).segment(0, n[0]).sum();
				sums[1] = data.col(pred).segment(start, n[1]).sum();
				sums[2] = data.col(pred).segment(end, n[2]).sum();
				assert(sums[0] + sums[1] + sums[2] == data.col(pred).sum());
				for(int64_t j = 0; j < 3; j++) {
					logPOneNiche += lbeta(sums[j] + pAlpha[j], nSpecies - sums[j] + pBeta[j]);
					logPOneNiche -= lbeta(pAlpha[j], pBeta[j]);
				}
				
				if(logPOnePred == -INFINITY) {
					logPOnePred = logPOneNiche;
				}
				else {
					logPOnePred = logSumExp(logPOnePred, logPOneNiche);
				}
//				cerr << "logPOneNiche: " << logPOneNiche << endl;
			}
		}
//		cerr << "logPOnePred: " << logPOnePred << endl;
		logP += logPOnePred;
	}
	cerr << "logP: " << logP << endl;
	
	double logP2 = model.logP();
	REQUIRE(logP2 > logP - tol);
	REQUIRE(logP2 < logP + tol);
}

TEST_CASE("niche test MCMC run", "[NicheModel]")
{
	random_device rd;
	bernoulli_distribution bd(0.4);
	
	double const tol = 1e-3;
	
	int64_t nSpecies = 10;
	MatrixXi64 data(nSpecies, nSpecies);
	for(int64_t i = 0; i < nSpecies; i++) {
		for(int64_t j = 0; j < nSpecies; j++) {
			data(i,j) = bd(rd);
		}
	}
	
	cerr << data << endl;
	NicheModel model(&data);
	
	zppsim::rng_t rng;
	
	MCMC mcmc(&model, &rng, 10);
	cerr << "initial proposal covariance: " << endl;
	cerr << mcmc.proposalCovariance() << endl;
	
	double acceptanceRate;
	
	MCMC::SampleCallback callback([&model](int64_t index, VectorXd params) {
//		cerr << model.logP() << endl;
	});
	
	/*for(int64_t i = 0; i < 10; i++) {
		mcmc.runDiscrete(100);
		cerr << model.reorderedData() << endl;
		cerr << model.logP() << endl;
	}*/
	
	int64_t nTries = 0;
	do {
		acceptanceRate = mcmc.tune(100, callback);
		cerr << "acceptance rate: " << acceptanceRate << endl;
		cerr << "params: " << model.parameters().transpose() << endl;
		cerr << "logP: " << model.logP() << endl;
		cerr << "reorderedData: " << endl << model.reorderedData() << endl;
		
		cerr << "proposal multiplier: " << mcmc.proposalMultiplier() << endl;
		cerr << "proposal covariance: " << endl;
		cerr << mcmc.proposalCovariance() << endl;
		
		nTries++;
	} while(nTries < 20 || acceptanceRate <= 0.10 || acceptanceRate >= 0.50);
	
	cerr << "nTries: " << nTries << endl;
	
	REQUIRE(acceptanceRate < 0.50);
	REQUIRE(acceptanceRate > 0.10);
	
	/*int64_t nSamples = 10000;
	MatrixXd samples(covariance.rows(), nSamples);
	acceptanceRate = mcmc.run(nSamples,
		[&samples](int64_t index, Eigen::VectorXd const & sample) {
			samples.col(index) = sample;
		}
	);
	MatrixXd sampleCovariance = unbiasedCovariance(samples);
	cerr << "sample covariance: " << endl;
	cerr << sampleCovariance << endl;
	
	double error = normalizedEntryError(covariance, sampleCovariance);
	cerr << "error: " << error << endl;
	REQUIRE(error < 3e-2);*/
}
