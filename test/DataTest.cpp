#include "catch.hpp"
#include "Config.hpp"
#include <iostream>
#include <Eigen/Dense>
#include "Database.hpp"
#include "netgroups_util.hpp"

using namespace std;
using namespace Eigen;
using namespace zppdata;

TEST_CASE("Config loading", "[Config]")
{
	Config config = loadConfig("config.json");
	config.verify();
	cerr << config.dumpToJson() << endl;
	
	REQUIRE(config.model == "niche");
	REQUIRE(config.data == "serengeti_foodweb.sqlite");
	
	REQUIRE(config.supergroups.size() == 3);
	REQUIRE(config.supergroups[0].id == "plants");
	REQUIRE(config.supergroups[0].nGroups == 1);
	
	REQUIRE(config.supergroups[1].id == "herbivores");
	REQUIRE(config.supergroups[1].nGroups == 1);
	
	REQUIRE(config.supergroups[2].id == "carnivores");
	REQUIRE(config.supergroups[2].nGroups == 1);
	
	REQUIRE(config.blocks.size() == 3);
	REQUIRE(config.blocks[0].name == "ph");
	REQUIRE(config.blocks[0].from == "plants");
	REQUIRE(config.blocks[0].to == "herbivores");
	REQUIRE(config.blocks[1].name == "hc");
	REQUIRE(config.blocks[1].from == "herbivores");
	REQUIRE(config.blocks[1].to == "carnivores");
	REQUIRE(config.blocks[2].name == "cc");
	REQUIRE(config.blocks[2].from == "carnivores");
	REQUIRE(config.blocks[2].to == "carnivores");
}

TEST_CASE("Data loading", "[loadNetwork]")
{
	MatrixXi64 herbCarnCorrect(23,9);
	herbCarnCorrect <<
		1, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 0, 1, 0, 1, 1, 1,
		1, 1, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 1, 0, 0, 1, 0, 1, 1, 1,
		1, 1, 0, 0, 1, 0, 1, 1, 1,
		1, 1, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 1,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		1, 0, 1, 1, 1, 0, 1, 1, 1,
		1, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 0, 0, 1,
		0, 0, 0, 1, 0, 0, 0, 0, 1,
		1, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 1, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 1, 0, 1, 1, 1,
		0, 0, 0, 1, 0, 1, 1, 1, 1,
		0, 0, 0, 0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 0, 0, 1, 1,
		0, 0, 0, 0, 0, 0, 1, 1, 1
	;
	
	Database db("serengeti_foodweb.sqlite");
	MatrixXi64 herbCarnLoaded = loadNetwork(db, "herbivores", "carnivores");
	cerr << herbCarnLoaded << endl;
	
	REQUIRE(herbCarnLoaded == herbCarnCorrect);
}
