#include "catch.hpp"
#include "netgroups_util.hpp"
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;
using namespace std;

#define TOL 1e-7

bool checkMatrices(MatrixXd X1, MatrixXd X2)
{
	REQUIRE(X1.rows() == X2.rows());
	REQUIRE(X1.cols() == X2.cols());
	
	for(int64_t i = 0; i < X1.rows(); i++) {
		for(int64_t j = 0; j < X1.cols(); j++) {
			REQUIRE(fabs(X1(i,j) - X2(i,j)) < TOL);
		}
	}
	return true;
}

TEST_CASE("permutationMatrix test", "[permutationMatrix]")
{
	VectorXi64 order(5);
	order << 1, 3, 4, 2, 0;
	
	MatrixXd X(5,5);
	X <<
		0, 1, 2, 3, 4,
		5, 6, 7, 8, 9,
		10, 11, 12, 13, 14,
		15, 16, 17, 18, 19,
		20, 21, 22, 23, 24
	;
	
	MatrixXd permutedXCheck(5,5);
	permutedXCheck <<
		 6,  8,  9,  7,  5,
		16, 18, 19, 17, 15,
		21, 23, 24, 22, 20,
		11, 13, 14, 12, 10,
		 1,  3,  4,  2,  0
	;
	
	MatrixXd permutedX = permuteMatrix(X, order);
	cerr << permutedX << endl;
	REQUIRE(permutedX == permutedXCheck);
}

TEST_CASE("unbiasedCovariance test", "[unbiasedCovariance]")
{
	MatrixXd X(5,4);
	X <<
		1.00, 1.10, 1.05, 1.03,
		1.50, 0.90, 3.70, 4.90,
		0.37, 0.90, 0.67, 0.12,
		0.34, 0.10, 0.57, 0.34,
		0.31, 0.94, 0.47, 0.56
	;
	MatrixXd C = unbiasedCovariance(X);
	
	MatrixXd correctC(5,5);
	correctC <<
		 0.0017666666666666699, -0.02433333333333336,  0.011466666666666675, -0.00401666666666667,  0.010566666666666677,
		-0.02433333333333336,    3.503333333333334,   -0.41100000000000003,   0.22083333333333335, -0.1586666666666666,
		 0.011466666666666675,  -0.41100000000000003,  0.11643333333333335,  -0.018916666666666672, 0.05619999999999999,
		-0.00401666666666667,    0.22083333333333335, -0.018916666666666672,  0.03682499999999999, -0.03726666666666666,
		 0.010566666666666677,  -0.1586666666666666,   0.05619999999999999,  -0.03726666666666666,  0.07153333333333332
	;
	
	REQUIRE(checkMatrices(C, correctC));
}
