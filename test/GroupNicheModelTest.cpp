#include "catch.hpp"
#include <vector>
#include <iostream>
#include <random>
#include "netgroups_util.hpp"
#include "MCMC.hpp"
#include "GroupNicheModel.hpp"

using namespace std;
using namespace Eigen;
using namespace zppdata;

/*TEST_CASE("construct group-niche model from data", "[GroupNicheModel]")
{
	Config config = loadConfig("config.json");
	Database db(config.data);
	
	GroupNicheModel model(config, db);
	cerr << model.logP() << endl;
}

TEST_CASE("group-niche model test run", "[GroupNicheModel]")
{
	Config config = loadConfig("config.json");
	Database db(config.data);
	
	GroupNicheModel model(config, db);
	cerr << model.logP() << endl;
	
	zppsim::rng_t rng;
	
	MCMC mcmc(&model, &rng, 1);
	cerr << "initial proposal covariance: " << endl;
	cerr << mcmc.proposalCovariance() << endl;
	
	double acceptanceRate;
	
	MCMC::SampleCallback callback([&model](int64_t index, VectorXd params) {
		cerr << model.logP() << endl;
		cerr << model.parameters().transpose() << endl;
	});
	
	int64_t nTries = 0;
	do {
		acceptanceRate = mcmc.tune(100, callback);
		cerr << "acceptance rate: " << acceptanceRate << endl;
		cerr << "params: " << model.parameters().transpose() << endl;
		cerr << "logP: " << model.logP() << endl;
//		cerr << "reorderedData: " << endl << model.reorderedData() << endl;
		
		cerr << "proposal multiplier: " << mcmc.proposalMultiplier() << endl;
		cerr << "proposal covariance: " << endl;
		cerr << mcmc.proposalCovariance() << endl;
		
		nTries++;
	} while(nTries < 20 || acceptanceRate <= 0.10 || acceptanceRate >= 0.50);
	
	cerr << "nTries: " << nTries << endl;
	
	REQUIRE(acceptanceRate < 0.50);
	REQUIRE(acceptanceRate > 0.10);
}*/

TEST_CASE("test with groups", "[GroupNicheModel]")
{
	Config config = loadConfig("config_groups.json");
	Database db(config.data);
	
	GroupNicheModel model(config, db);
	cerr << model.logP() << endl;
	
	zppsim::rng_t rng;
	
	MCMC mcmc(&model, &rng, 1);
	cerr << "initial proposal covariance: " << endl;
	cerr << mcmc.proposalCovariance() << endl;
	
	double acceptanceRate;
	
	MCMC::SampleCallback callback([&model](int64_t index, VectorXd params) {
		cerr << model.logP() << endl;
		cerr << model.parameters().transpose() << endl;
	});
	
	int64_t nTries = 0;
	do {
		acceptanceRate = mcmc.tune(100, callback);
		cerr << "acceptance rate: " << acceptanceRate << endl;
		cerr << "params: " << model.parameters().transpose() << endl;
		cerr << "logP: " << model.logP() << endl;
//		cerr << "reorderedData: " << endl << model.reorderedData() << endl;
		
		cerr << "proposal multiplier: " << mcmc.proposalMultiplier() << endl;
		cerr << "proposal covariance: " << endl;
		cerr << mcmc.proposalCovariance() << endl;
		
		nTries++;
	} while(nTries < 20 || acceptanceRate <= 0.10 || acceptanceRate >= 0.50);
	
	cerr << "nTries: " << nTries << endl;
	
	REQUIRE(acceptanceRate < 0.50);
	REQUIRE(acceptanceRate > 0.10);
}

