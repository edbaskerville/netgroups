#include "catch.hpp"
#include "MCMC.hpp"
#include "Model.hpp"
#include <Eigen/Dense>
#include "zppsim_random.hpp"
#include "netgroups_util.hpp"

using namespace std;
using namespace Eigen;
using namespace zppsim;

class MVNormalModel : public Model
{
public:
	MVNormalModel(MatrixXd covariance, MatrixXd covarianceGuess) :
		_mvNormDist(covariance),
		_parameters(VectorXd::Zero(covariance.rows())),
		_covarianceGuess(covarianceGuess)
	{
	}
	
	virtual Eigen::VectorXd parameters()
	{
		return _parameters;
	}
	
	virtual double logP()
	{
		return _mvNormDist.logP(_parameters);
	}
	
	virtual Eigen::MatrixXd covarianceGuess()
	{
		return _covarianceGuess;
	}
	
	virtual void propose(Eigen::VectorXd const & newParams)
	{
		_parameters = newParams;
	}
	
	virtual void accept(Eigen::VectorXd const & newParams)
	{
	}
	
	virtual void reject(Eigen::VectorXd const & oldParams)
	{
		_parameters = oldParams;
	}
private:
	MVNormalDistribution _mvNormDist;
	VectorXd _parameters;
	MatrixXd _covarianceGuess;
};

TEST_CASE("multivariate MCMC too-large initial proposals", "[MCMC]")
{
	rng_t rng;
	
	MatrixXd covariance(5,5);
	covariance <<
		 1.0017666666666666699, -0.02433333333333336,  0.011466666666666675, -0.00401666666666667,  0.010566666666666677,
		-0.02433333333333336,    3.503333333333334,   -0.41100000000000003,   0.22083333333333335, -0.1586666666666666,
		 0.011466666666666675,  -0.41100000000000003,  1.11643333333333335,  -0.018916666666666672, 0.05619999999999999,
		-0.00401666666666667,    0.22083333333333335, -0.018916666666666672,  1.03682499999999999, -0.03726666666666666,
		 0.010566666666666677,  -0.1586666666666666,   0.05619999999999999,  -0.03726666666666666,  1.07153333333333332
	;
	
	MatrixXd covarianceGuess(5,5);
	covarianceGuess.diagonal() << 100, 501, 300, 40, 100;
	
	MVNormalModel model(covariance, covarianceGuess);
	
	MCMC mcmc(&model, &rng, 10);
	cerr << "initial proposal covariance: " << endl;
	cerr << mcmc.proposalCovariance() << endl;
	
	double acceptanceRate;
	for(int i = 0; i < 20; i++) {
		acceptanceRate = mcmc.tune(100*(i+1));
		
		cerr << "acceptance rate: " << acceptanceRate << endl;
		
		cerr << "proposal covariance: " << endl;
		cerr << mcmc.proposalCovariance() << endl;
	}
	REQUIRE(acceptanceRate < 0.35);
	REQUIRE(acceptanceRate > 0.16);
	
	int64_t nSamples = 10000;
	MatrixXd samples(covariance.rows(), nSamples);
	acceptanceRate = mcmc.run(nSamples,
		[&samples](int64_t index, Eigen::VectorXd const & sample) {
			samples.col(index) = sample;
		}
	);
	MatrixXd sampleCovariance = unbiasedCovariance(samples);
	cerr << "sample covariance: " << endl;
	cerr << sampleCovariance << endl;
	
	double error = normalizedEntryError(covariance, sampleCovariance);
	cerr << "error: " << error << endl;
	REQUIRE(error < 3e-2);
}

TEST_CASE("multivariate MCMC too-small initial proposals", "[MCMC]")
{
	rng_t rng;
	
	MatrixXd covariance(5,5);
	covariance <<
		 1.0017666666666666699, -0.02433333333333336,  0.011466666666666675, -0.00401666666666667,  0.010566666666666677,
		-0.02433333333333336,    3.503333333333334,   -0.41100000000000003,   0.22083333333333335, -0.1586666666666666,
		 0.011466666666666675,  -0.41100000000000003,  1.11643333333333335,  -0.018916666666666672, 0.05619999999999999,
		-0.00401666666666667,    0.22083333333333335, -0.018916666666666672,  1.03682499999999999, -0.03726666666666666,
		 0.010566666666666677,  -0.1586666666666666,   0.05619999999999999,  -0.03726666666666666,  1.07153333333333332
	;
	
	MatrixXd covarianceGuess = MatrixXd::Identity(5,5);
	
	MVNormalModel model(covariance, covarianceGuess);
	
	MCMC mcmc(&model, &rng, 10);
	cerr << "initial proposal covariance: " << endl;
	cerr << mcmc.proposalCovariance() << endl;
	
	double acceptanceRate;
	do {
		acceptanceRate = mcmc.tune(100);
	} while(acceptanceRate >= 0.99);
	
	for(int i = 0; i < 20; i++) {
		acceptanceRate = mcmc.tune(1000);
		cerr << "acceptance rate: " << acceptanceRate << endl;
		
		cerr << "proposal covariance: " << endl;
		cerr << mcmc.proposalCovariance() << endl;
	}
	REQUIRE(acceptanceRate < 0.35);
	REQUIRE(acceptanceRate > 0.16);
	
	int64_t nSamples = 10000;
	MatrixXd samples(covariance.rows(), nSamples);
	acceptanceRate = mcmc.run(nSamples,
		[&samples](int64_t index, Eigen::VectorXd const & sample) {
			samples.col(index) = sample;
		}
	);
	MatrixXd sampleCovariance = unbiasedCovariance(samples);
	cerr << "sample covariance: " << endl;
	cerr << sampleCovariance << endl;
	
	double error = normalizedEntryError(covariance, sampleCovariance);
	cerr << "error: " << error << endl;
	REQUIRE(error < 3e-2);
}

