#include "catch.hpp"
#include "netgroups_util.hpp"
#include "MVNormalDistribution.hpp"

#include <Eigen/Dense>
#include <iostream>

using namespace std;
using namespace Eigen;

TEST_CASE("test multivariate normal generation", "[MVNormalDistribution]")
{
	VectorXd mean(5);
	mean <<
		0.1,
		0.2,
		0.3,
		0.4,
		0.5
	;
	
	MatrixXd covariance(5,5);
	covariance <<
		 1.0017666666666666699, -0.02433333333333336,  0.011466666666666675, -0.00401666666666667,  0.010566666666666677,
		-0.02433333333333336,    3.503333333333334,   -0.41100000000000003,   0.22083333333333335, -0.1586666666666666,
		 0.011466666666666675,  -0.41100000000000003,  1.11643333333333335,  -0.018916666666666672, 0.05619999999999999,
		-0.00401666666666667,    0.22083333333333335, -0.018916666666666672,  1.03682499999999999, -0.03726666666666666,
		 0.010566666666666677,  -0.1586666666666666,   0.05619999999999999,  -0.03726666666666666,  1.07153333333333332
	;
	
	MVNormalDistribution mvNormalDist(mean, covariance);
	
	int64_t nSamples = 10000;
	
	random_device rd;
	MatrixXd samples(mean.rows(), nSamples);
	for(int64_t i = 0; i < nSamples; i++) {
		samples.col(i) = mvNormalDist(rd);
	}
	
	VectorXd sampleMean = samples.rowwise().mean();
	cerr << sampleMean << endl;
	
	MatrixXd sampleCovariance = unbiasedCovariance(samples);
	cerr << sampleCovariance << endl;
	double error = normalizedEntryError(covariance, sampleCovariance);
	cerr << error << endl;
	
	REQUIRE(error < 5e-2);
}
