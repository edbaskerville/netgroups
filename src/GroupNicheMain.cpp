//
//  Main.cpp
//  netgroups
//
//  Created by Ed Baskerville on 8/5/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "GroupNicheMain.hpp"

using namespace std;
using namespace zppsim;
using namespace zppdata;
using namespace Eigen;

GroupNicheMain::GroupNicheMain(Config & config) :
	config(config),
	dataDb(config.dataFilename),
	outputDb(config.outputFilename),
	rng(random_device()()),
	model(config, dataDb),
	mcmc(&model, &rng, config.thin),
	logPTable(&outputDb, "logP",
		{
			{"iteration", DBType::INTEGER},
			{"logP", DBType::REAL}
		}
	),
	tuningTable(&outputDb, "tuning",
		{
			{"round", DBType::INTEGER},
			{"acceptanceRate", DBType::REAL},
			{"proposalMultiplier", DBType::REAL}
		}
	),
	covarianceTable(&outputDb, "proposalCovariance",
		{
			{"round", DBType::INTEGER},
			{"row", DBType::INTEGER},
			{"col", DBType::INTEGER},
			{"covariance", DBType::REAL}
		}
	),
	orderTable(&outputDb, "nicheOrder",
		{
			{"iteration", DBType::INTEGER},
			{"supergroupId", DBType::TEXT},
			{"position", DBType::INTEGER},
			{"speciesIndex", DBType::INTEGER},
			{"speciesId", DBType::TEXT}
		}
	),
	groupTable(&outputDb, "groups",
		{
			{"iteration", DBType::INTEGER},
			{"supergroupId", DBType::TEXT},
			{"dividerIndex", DBType::INTEGER},
			{"dividerPosition", DBType::INTEGER}
		}
	),
	blockParamTable(&outputDb, "blockParams",
		{
			{"iteration", DBType::INTEGER},
			{"fromId", DBType::TEXT},
			{"toId", DBType::TEXT},
			{"paramName", DBType::TEXT},
			{"value", DBType::REAL}
		}
	),
	blockGroupParamTable(&outputDb, "blockGroupParams",
		{
			{"iteration", DBType::INTEGER},
			{"fromId", DBType::TEXT},
			{"toId", DBType::TEXT},
			{"groupId", DBType::INTEGER},
			{"paramName", DBType::TEXT},
			{"value", DBType::REAL}
		}
	)
{
	logPTable.create();
	tuningTable.create();
	covarianceTable.create();
	orderTable.create();
	groupTable.create();
	blockParamTable.create();
	blockGroupParamTable.create();
}

void GroupNicheMain::run()
{
	double acceptanceRate;
	
	// Initial state
	_writeOutput(0);
	
	int64_t round = 1;
	int64_t nextStop = config.nDiscreteRounds;
	
	// Discrete-only warmup rounds
	for(; round <= nextStop; round++) {
		mcmc.runDiscrete(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index);
			}
		);
	}
	
	
	// Tuning rounds
	nextStop += config.nTuningRounds;
	for(; round <= nextStop; round++) {
		acceptanceRate = mcmc.tune(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index);
			}
		);
		
		DBRow tuningRow;
		tuningRow.setInteger("round", round);
		tuningRow.setReal("acceptanceRate", acceptanceRate);
		tuningRow.setReal("proposalMultiplier", mcmc.proposalMultiplier());
		tuningTable.insert(tuningRow);
		
		MatrixXd covariance = mcmc.proposalCovariance();
		for(int64_t i = 0; i < covariance.rows(); i++) {
			for(int64_t j = i; j < covariance.rows(); j++) {
				DBRow covarianceRow;
				covarianceRow.setInteger("round", round);
				covarianceRow.setInteger("row", i);
				covarianceRow.setInteger("col", j);
				covarianceRow.setReal("covariance", covariance(i,j));
				covarianceTable.insert(covarianceRow);
			}
		}
	}
	
	// Regular rounds
	nextStop += config.nMarkovRounds;
	for(; round <= nextStop; round++) {
		acceptanceRate = mcmc.run(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index);
			}
		);
		
		DBRow tuningRow;
		tuningRow.setInteger("round", round);
		tuningRow.setReal("acceptanceRate", acceptanceRate);
		tuningRow.setReal("proposalMultiplier", mcmc.proposalMultiplier());
		tuningTable.insert(tuningRow);
	}
}

void GroupNicheMain::_writeOutput(int64_t iteration)
{
	_writeLogP(iteration);
	_writeOrderParams(iteration);
	_writeGroupParams(iteration);
	_writeBlockParams(iteration);
	_writeBlockGroupParams(iteration);
}

void GroupNicheMain::_writeLogP(int64_t iteration)
{
	DBRow row;
	row.setInteger("iteration", iteration);
	row.setReal("logP", model.logP());
	logPTable.insert(row);
}

void GroupNicheMain::_writeOrderParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		VectorXi64 order = model.getSupergroupOrder(i);
		for(int64_t j = 0; j < order.size(); j++) {
			DBRow row;
			row.setInteger("iteration", iteration);
			row.setText("supergroupId", config.supergroups[i].id);
			row.setInteger("position", j);
			row.setInteger("speciesIndex", order[j]);
			row.setText("speciesId", model.getSpeciesId(i, order[j]));
			orderTable.insert(row);
		}
	}
}

void GroupNicheMain::_writeGroupParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		VectorXi64 dividers = model.getSupergroupDividers(i);
		for(int64_t j = 0; j < dividers.size(); j++) {
			DBRow row;
			row.setInteger("iteration", iteration);
			row.setText("supergroupId", config.supergroups[i].id);
			row.setInteger("dividerIndex", j);
			row.setInteger("dividerPosition", dividers[j]);
			groupTable.insert(row);
		}
	}
}

void GroupNicheMain::_writeBlockParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.blocks.size(); i++) {
		DBRow row;
		row.setInteger("iteration", iteration);
		row.setText("fromId", config.blocks[i].from);
		row.setText("toId", config.blocks[i].to);
		for(auto kvPair : model.getBlockParams(i)) {
			row.setText("paramName", kvPair.first);
			row.setReal("value", kvPair.second);
			blockParamTable.insert(row);
		}
	}
}

void GroupNicheMain::_writeBlockGroupParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.blocks.size(); i++) {
		DBRow row;
		row.setInteger("iteration", iteration);
		row.setText("fromId", config.blocks[i].from);
		row.setText("toId", config.blocks[i].to);
		for(int64_t j = 0; j < model.fromGroupCount(i); j++) {
			row.setInteger("groupId", j);
			for(auto kvPair : model.getBlockGroupParams(i, j)) {
				row.setText("paramName", kvPair.first);
				row.setReal("value", kvPair.second);
				blockGroupParamTable.insert(row);
			}
		}
	}
}
