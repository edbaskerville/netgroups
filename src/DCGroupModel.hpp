#ifndef __netgroups__DCGroupModel__
#define __netgroups__DCGroupModel__

#include <vector>
#include <Eigen/Dense>
#include <boost/dynamic_bitset.hpp>
#include "dagoberto.hpp"
#include "MatrixMultiplexer.hpp"
#include "Model.hpp"
#include "Config.hpp"
#include "Database.hpp"
#include "netgroups_util.hpp"
#include <unordered_set>
#include <memory>

namespace dcgroup
{

struct Supergroup
{
	Supergroup(zppdata::Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph);
	
	std::string id;
	int64_t nGroups;
	int64_t index;
	std::vector<std::string> nodeIds;
	int64_t size;
	std::unordered_map<std::string, int64_t> nodeIndexes;
	
	std::vector<dagoberto::Value<int64_t>> assignmentNodes;
	std::vector<dagoberto::Value<boost::dynamic_bitset<>>> groupNodes;
};


/********* NEW MODEL TO DO *************

group-pair parameters in matrices

predator prior parameters are a function of assignment, group-pair matrices
in order to be properly updated at continuous-param proposal time

group proposals thus cause updates to predator prior parameters

group proposals must also update link counts, which exist per-predator,
per prey group, per block. done manually.

*********************/

struct NetBlock
{
	NetBlock(zppdata::Database & db, Supergroup & sgFrom, Supergroup & sgTo, dagoberto::Graph & logPGraph);
	
	void appendParams(std::vector<dagoberto::Value<double> *> & params);
	void appendLogPTerms(std::vector<dagoberto::Node<double> *> & logPTerms);
	
	int64_t getColLinkCount(boost::dynamic_bitset<> const & fromIndexes, int64_t toIndex);
	
	Supergroup * sgFromPtr;
	Supergroup * sgToPtr;
	
	MatrixXi64 data;
	
	dagoberto::Value<double> pAlphaLambdaNode;
	dagoberto::Value<double> pBetaLambdaNode;
	
	std::vector<std::vector<dagoberto::Value<double>>> pAlphaNodes;
	std::vector<std::vector<dagoberto::Value<double>>> pBetaNodes;
	
	std::unique_ptr<dagoberto::MatrixMultiplexer<double>> pAlphaMatrixNode;
	std::unique_ptr<dagoberto::MatrixMultiplexer<double>> pBetaMatrixNode;
	
	std::vector<std::vector<dagoberto::Function<double>>> predPAlphaNodes;
	std::vector<std::vector<dagoberto::Function<double>>> predPBetaNodes;
	std::vector<std::vector<dagoberto::Function<double>>> predLinkLogPs;
	
	std::unique_ptr<dagoberto::Function<double>> paramLogPNode;
};

class DCGroupModel : public Model
{
public:
	DCGroupModel(Config & config, zppdata::Database & db);
	
	void verify();
	
	virtual Eigen::VectorXd parameters();
	virtual double logP();
	
	virtual Eigen::MatrixXd covarianceGuess();
	
	virtual void propose(Eigen::VectorXd const & newParams);
	virtual void accept(Eigen::VectorXd const & newParams);
	virtual void reject(Eigen::VectorXd const & oldParams);
	
	virtual void iterateDiscrete(zppsim::rng_t & rng);
	
	std::string getSpeciesId(int64_t sgIndex, int64_t speciesIndex);
	int64_t speciesCount(int64_t sgIndex);
	VectorXi64 getAssignments(int64_t sgIndex);
	
	std::vector<std::pair<std::string, double>> getBlockParams(int64_t blockIndex);
	
	std::int64_t getPreyGroupCount(int64_t blockIndex);
	std::int64_t getPredGroupCount(int64_t blockIndex);
	std::vector<std::pair<std::string, double>> getGroupPairParams(int64_t blockIndex, int64_t gi, int64_t gj);
	
private:
	void _iterateGrouping(int64_t sgId, zppsim::rng_t & rng);
	void _proposeGroupMove(int64_t sgId, int64_t nodeIndex, int64_t groupIndex, zppsim::rng_t & rng);
	
	dagoberto::Graph _logPGraph;
	std::vector<Supergroup> _supergroups;
	std::vector<NetBlock> _blocks;
	
	std::vector<dagoberto::Value<double> *> _params;
	
	std::unique_ptr<dagoberto::VectorSum<double>> _logPRoot;
};

} // namespace dcgroup

#endif /* defined(__netgroups__DCGroupModel__) */
