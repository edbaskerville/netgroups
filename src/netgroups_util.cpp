#include "netgroups_util.hpp"
#include "zppdata_util.hpp"
#include <iostream>
#include <unordered_map>
#include "zppsim_random.hpp"

using namespace std;
using namespace Eigen;
using namespace zppdata;

Eigen::MatrixXd unbiasedCovariance(Eigen::MatrixXd X)
{
	if(X.cols() < 2) {
		return MatrixXd::Zero(X.rows(), X.rows());
	}
	
	VectorXd xMean = X.rowwise().mean();
	assert(xMean.rows() == X.rows());
	
	Eigen::MatrixXd Xdiff = X - xMean * MatrixXd::Ones(1, X.cols());
	return Xdiff * Xdiff.transpose() / double(X.cols() - 1);
}

double normalizedEntryError(Eigen::MatrixXd xCorrect, Eigen::MatrixXd xWrong)
{
	MatrixXd xDiff = xCorrect - xWrong;
	return xDiff.norm() / xCorrect.norm();
}

MatrixXd logMatrix(MatrixXd const & x)
{
	MatrixXd logx(x.rows(), x.cols());
	for(int64_t i = 0; i < x.rows(); i++) {
		for(int64_t j = 0; j < x.rows(); j++) {
			logx(i,j) = log(x(i,j));
		}
	}
	return logx;
}

VectorXd logVector(VectorXd const & x)
{
	VectorXd logx(x.size());
	for(int64_t i = 0; i < x.size(); i++) {
		logx[i] = log(x[i]);
	}
	return logx;
}

MatrixXd expMatrix(MatrixXd const & x)
{
	MatrixXd expx(x.rows(), x.cols());
	for(int64_t i = 0; i < x.rows(); i++) {
		for(int64_t j = 0; j < x.rows(); j++) {
			expx(i,j) = exp(x(i,j));
		}
	}
	return expx;
}

VectorXd expVector(VectorXd const & x)
{
	VectorXd expx(x.size());
	for(int64_t i = 0; i < x.size(); i++) {
		expx[i] = exp(x[i]);
	}
	return expx;
}

double logSumExp(double a, double b)
{
	double c = max(a, b);
	return c + log(exp(a-c) + exp(b-c));
}

double lbeta(double x, double y)
{
	return lgamma(x) + lgamma(y) - lgamma(x + y);
}

MatrixXd permuteMatrix(MatrixXd const & X, VectorXi64 const & indexOrder)
{
	auto P = PermutationMatrix<Dynamic,Dynamic,int64_t>(indexOrder);
	MatrixXd xRowsPermuted = P.transpose() * X;
	MatrixXd xPermuted = xRowsPermuted * P;
	return xPermuted;
}

MatrixXi64 permuteMatrix(MatrixXi64 const & X, VectorXi64 const & indexOrder)
{
	auto P = PermutationMatrix<Dynamic,Dynamic,int64_t>(indexOrder);
	MatrixXi64 xRowsPermuted = P.transpose() * X;
	MatrixXi64 xPermuted = xRowsPermuted * P;
	return xPermuted;
}

double exponentialLogP(double const & lambda, double const & x)
{
	if(std::isinf(lambda) || lambda <= 0) {
		return nan("");
	}
	
	if(std::isinf(x) || x < 0) {
		return -INFINITY;
	}
	return -lambda * x + log(lambda);
}

double exponentialLogPVector(double const & lambda, Eigen::VectorXd const & x)
{
	double logP = 0.0;
	for(int64_t i = 0; i < x.size(); i++) {
		logP += exponentialLogP(lambda, x[i]);
	}
	return logP;
}

double dirichletMultinomialLogP(Eigen::VectorXd const & alphaVec, VectorXi64 & nVec)
{
	if(!isPositive(alphaVec)) {
		return nan("");
	}
	
	double k = nVec.size();
	assert(alphaVec.size() == k);
	double N = nVec.sum();
	double A = alphaVec.sum();
	
	double logP = 0.0;
	logP += log(N) + lbeta(A, N);
	for(int64_t i = 0; i < k; i++) {
		if(nVec[i] < 0) {
			return -INFINITY;
		}
		
		if(nVec[i] > 0) {
			logP -= log(nVec[i]) + lbeta(alphaVec[i], nVec[i]);
		}
	}
	return logP;
}

double betaBernoulliLogP(double alpha, double beta, int64_t nSuccesses, int64_t nFailures)
{
	if(!isPositive(alpha) || !isPositive(beta)) {
		return nan("");
	}
	
	if(nSuccesses < 0 || nFailures < 0) {
		return -INFINITY;
	}
	
	return lbeta(nSuccesses + alpha, nFailures + beta) - lbeta(alpha, beta);
}

VectorXi64 assignmentsFromDividers(VectorXi64 const & dividers)
{
	assert(dividers[0] == 0);
	int64_t size = dividers[dividers.size() - 1];
	
	VectorXi64 assignments(size);
	for(int64_t g = 0; g < dividers.size() - 1; g++) {
		assert(dividers[g] < dividers[g+1]);
		for(int64_t i = dividers[g]; i < dividers[g+1]; i++) {
			assignments[i] = (int)g;
		}
	}
	return assignments;
}

std::vector<std::string> loadSupergroup(zppdata::Database & db, std::string const & supergroup)
{
	vector<string> members;
	DBQuery query(&db, "SELECT nodeId FROM nodes WHERE supergroupId == ?");
	query.bindText(0, supergroup);
	while(query.step() == SQLITE_ROW) {
		members.push_back(query.getText(0));
	}
	return members;
}

MatrixXi64 loadNetwork(zppdata::Database & db, std::string const & fromSupergroup, std::string const & toSupergroup)
{
	auto fromMembers = loadSupergroup(db, fromSupergroup);
	auto fromIndexes = makeIndexMap(fromMembers);
	auto toMembers = loadSupergroup(db, toSupergroup);
	auto toIndexes = makeIndexMap(toMembers);
	
	MatrixXi64 adjMat = MatrixXi64::Zero(fromMembers.size(), toMembers.size());
	DBQuery query(&db, strprintf("SELECT * FROM links"));
	while(query.step() == SQLITE_ROW) {
		string from = query.getText(0);
		string to = query.getText(1);
		auto fromItr = fromIndexes.find(from);
		auto toItr = toIndexes.find(to);
		if(fromItr != fromIndexes.end() && toItr != toIndexes.end()) {
			adjMat(fromItr->second, toItr->second) = 1;
		}
	}
	return adjMat;
}

bool isPositive(double x) {
	return std::isfinite(x) && x > 0;
}

bool isPositive(VectorXd x) {
	for(int64_t i = 0; i < x.size(); i++) {
		if(!isPositive(x[i])) {
			return false;
		}
	}
	return true;
}

bool shouldAcceptMHSymmetric(double oldLogP, double newLogP, zppsim::rng_t & rng)
{
	if(!std::isfinite(newLogP)) {
		return false;
	}
	
	if(newLogP >= oldLogP) {
		return true;
	}
	else {
		bernoulli_distribution flipCoin(exp(newLogP - oldLogP));
		return flipCoin(rng);
	}
}

std::vector<int64_t> randomPermutation(int64_t size, zppsim::rng_t & rng)
{
	vector<int64_t> vec;
	vec.reserve(size);
	for(int64_t i = 0; i < size; i++) {
		vec.push_back(i);
	}
	shuffle(vec.begin(), vec.end(), rng);
	return vec;
}
