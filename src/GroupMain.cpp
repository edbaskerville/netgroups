#include "GroupMain.hpp"

using namespace std;
using namespace zppsim;
using namespace zppdata;
using namespace Eigen;
using namespace group;

GroupMain::GroupMain(Config & config) :
	config(config),
	dataDb(config.dataFilename),
	outputDb(config.outputFilename),
	rng(random_device()()),
	model(config, dataDb),
	mcmc(&model, &rng, config.thin),
	logPTable(&outputDb, "logP",
		{
			{"iteration", DBType::INTEGER},
			{"logP", DBType::REAL}
		}
	),
	tuningTable(&outputDb, "tuning",
		{
			{"round", DBType::INTEGER},
			{"acceptanceRate", DBType::REAL},
			{"proposalMultiplier", DBType::REAL}
		}
	),
	covarianceTable(&outputDb, "proposalCovariance",
		{
			{"round", DBType::INTEGER},
			{"row", DBType::INTEGER},
			{"col", DBType::INTEGER},
			{"covariance", DBType::REAL}
		}
	),
	groupTable(&outputDb, "groups",
		{
			{"iteration", DBType::INTEGER},
			{"supergroupId", DBType::TEXT},
			{"speciesId", DBType::TEXT},
			{"groupIndex", DBType::INTEGER}
		}
	),
	blockParamTable(&outputDb, "blockParams",
		{
			{"iteration", DBType::INTEGER},
			{"fromId", DBType::TEXT},
			{"toId", DBType::TEXT},
			{"paramName", DBType::TEXT},
			{"value", DBType::REAL}
		}
	)
{
	logPTable.create();
	tuningTable.create();
	covarianceTable.create();
	groupTable.create();
	blockParamTable.create();
}

void GroupMain::run()
{
	double acceptanceRate;
	
	// Initial state
	_writeOutput(0);
	
	int64_t round = 0;
	int64_t nextStop = config.nDiscreteRounds;
	
	// Discrete-only warmup rounds
	for(; round < nextStop; round++) {
		mcmc.runDiscrete(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index + 1);
			}
		);
	}
	
	// Tuning rounds
	nextStop += config.nTuningRounds;
	for(; round < nextStop; round++) {
		acceptanceRate = mcmc.tune(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index + 1);
			}
		);
		
		DBRow tuningRow;
		tuningRow.setInteger("round", round);
		tuningRow.setReal("acceptanceRate", acceptanceRate);
		tuningRow.setReal("proposalMultiplier", mcmc.proposalMultiplier());
		tuningTable.insert(tuningRow);
		
		MatrixXd covariance = mcmc.proposalCovariance();
		for(int64_t i = 0; i < covariance.rows(); i++) {
			for(int64_t j = i; j < covariance.rows(); j++) {
				DBRow covarianceRow;
				covarianceRow.setInteger("round", round);
				covarianceRow.setInteger("row", i);
				covarianceRow.setInteger("col", j);
				covarianceRow.setReal("covariance", covariance(i,j));
				covarianceTable.insert(covarianceRow);
			}
		}
	}
	
	// Regular rounds
	nextStop += config.nMarkovRounds;
	for(; round < nextStop; round++) {
		acceptanceRate = mcmc.run(
			config.iterationsPerRound,
			[this, round](int64_t index, VectorXd params) {
				_writeOutput(round * config.iterationsPerRound + index + 1);
			}
		);
		
		DBRow tuningRow;
		tuningRow.setInteger("round", round);
		tuningRow.setReal("acceptanceRate", acceptanceRate);
		tuningRow.setReal("proposalMultiplier", mcmc.proposalMultiplier());
		tuningTable.insert(tuningRow);
	}
}

void GroupMain::_writeOutput(int64_t iteration)
{
	outputDb.beginTransaction();
	
	cerr << "Finished iteration " << iteration << endl;
	_writeLogP(iteration);
	_writeGroupParams(iteration);
	_writeBlockParams(iteration);
	
	outputDb.commitWithRetry(100000, 10000000, cerr);
}

void GroupMain::_writeLogP(int64_t iteration)
{
	DBRow row;
	row.setInteger("iteration", iteration);
	row.setReal("logP", model.logP());
	logPTable.insert(row);
}

void GroupMain::_writeGroupParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		VectorXi64 assignments = model.getAssignments(i);
		DBRow row;
		row.setInteger("iteration", iteration);
		row.setText("supergroupId", config.supergroups[i].id);
		for(int j = 0; j < model.speciesCount(i); j++) {
			row.setText("speciesId", model.getSpeciesId(i, j));
			row.setInteger("groupIndex", assignments[j]);
			groupTable.insert(row);
		}
	}
}

void GroupMain::_writeBlockParams(int64_t iteration)
{
	for(int64_t i = 0; i < config.blocks.size(); i++) {
		DBRow row;
		row.setInteger("iteration", iteration);
		row.setText("fromId", config.blocks[i].from);
		row.setText("toId", config.blocks[i].to);
		for(auto kvPair : model.getBlockParams(i)) {
			row.setText("paramName", kvPair.first);
			row.setReal("value", kvPair.second);
			blockParamTable.insert(row);
		}
	}
}
