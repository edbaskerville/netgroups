//
//  GroupNicheModel.cpp
//  netgroups
//
//  Created by Ed Baskerville on 8/4/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "GroupNicheModel.hpp"
#include "netgroups_util.hpp"
#include "zppsim_random.hpp"
#include <random>

using namespace std;
using namespace dagoberto;
using namespace Eigen;
using namespace zppdata;
using namespace zppsim;

namespace groupniche
{

Supergroup::Supergroup(Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph) :
	id(supergroupId),
	nGroups(nGroups),
	nodeIds(loadSupergroup(db, supergroupId)),
	nodeIndexes(makeIndexMap(nodeIds)),
	size(nodeIds.size()),
	order(VectorXi64::LinSpaced(size, 0, size - 1)),
	dividers(VectorXi64::Zero(nGroups + 1))
{
	dividers[0] = 0;
	int64_t dividerSeparation = size / nGroups;
	assert(dividerSeparation > 0);
	for(int64_t i = 1; i < nGroups; i++) {
		dividers[i] = (int)(dividerSeparation * i);
	}
	dividers[nGroups] = (int)size;
	
	VectorXi64 assignments = assignmentsFromDividers(dividers);
	assignmentNodes.reserve(size);
	for(int64_t i = 0; i < size; i++) {
		assignmentNodes.emplace_back(&logPGraph, assignments[i]);
	}
}

PredGroupParams::PredGroupParams(int64_t index, dagoberto::Graph & logPGraph) :
	index(index),
	alphaBelowNode(&logPGraph, 1.0),
	alphaNicheNode(&logPGraph, 1.0)
{
	paramLogPNode = unique_ptr<Function<double>>(new Function<double>(
		&logPGraph,
		{&alphaBelowNode, &alphaNicheNode},
		[this]() {
			double logP = 0.0;
			logP += exponentialLogP(1.0, alphaBelowNode());
			logP += exponentialLogP(1.0, alphaNicheNode());
			return logP;
		}
	));
}

void PredGroupParams::appendParams(std::vector<dagoberto::Value<double> *> & params)
{
	params.push_back(&alphaBelowNode);
	params.push_back(&alphaNicheNode);
}

NetBlock::NetBlock(
	zppdata::Database & db,
	Supergroup & sgFrom, Supergroup & sgTo,
	dagoberto::Graph & logPGraph
) :
	sgFromPtr(&sgFrom), sgToPtr(&sgTo),
	data(loadNetwork(db, sgFrom.id, sgTo.id)),
	alphaBelowSlopeNode(&logPGraph, 1.0),
	alphaNicheSlopeNode(&logPGraph, 1.0),
	alphaAboveNode(&logPGraph, 1.0),
	pAlphaBelowNode(&logPGraph, 1.0),
	pAlphaNicheNode(&logPGraph, 1.0),
	pAlphaAboveNode(&logPGraph, 1.0),
	pBetaNode(&logPGraph, 1.0)
{
//	cerr << data << endl << endl;
	
	colNodes.reserve(sgToPtr->size);
	for(int64_t i = 0; i < sgToPtr->size; i++) {
		colNodes.emplace_back(&logPGraph, data.col(i));
	}
	
	paramLogPNode = unique_ptr<Function<double>>(new Function<double>(
		&logPGraph,
		{&alphaBelowSlopeNode, &alphaNicheSlopeNode, &alphaAboveNode,
		&pAlphaBelowNode, &pAlphaNicheNode, &pAlphaAboveNode, &pBetaNode},
		[this]() {
			double logP = 0.0;
			
			logP += exponentialLogP(1.0, alphaBelowSlopeNode());
			logP += exponentialLogP(1.0, alphaNicheSlopeNode());
			logP += exponentialLogP(1.0, alphaAboveNode());
			
			logP += exponentialLogP(1.0, pAlphaBelowNode());
			logP += exponentialLogP(1.0, pAlphaNicheNode());
			logP += exponentialLogP(1.0, pAlphaAboveNode());
			logP += exponentialLogP(1.0, pBetaNode());
			
			return logP;
		}
	));
	
	// Parameters for each predator group
	predGroupParams.reserve(sgToPtr->nGroups);
	for(int64_t i = 0; i < sgToPtr->nGroups; i++) {
		predGroupParams.emplace_back(i, logPGraph);
	}
	
	// Log-p data calculation for each predator
	predLogPNodes.reserve(sgToPtr->size);
	for(int64_t i = 0; i < sgToPtr->size; i++) {
		vector<NodeBase *> dependencies;
		dependencies.push_back(&alphaBelowSlopeNode);
		dependencies.push_back(&alphaNicheSlopeNode);
		dependencies.push_back(&alphaAboveNode);
		dependencies.push_back(&pAlphaBelowNode);
		dependencies.push_back(&pAlphaNicheNode);
		dependencies.push_back(&pAlphaAboveNode);
		dependencies.push_back(&pBetaNode);
		for(auto & pgp : predGroupParams) {
			dependencies.push_back(&pgp.alphaBelowNode);
			dependencies.push_back(&pgp.alphaNicheNode);
		}
		dependencies.push_back(&sgToPtr->assignmentNodes[i]);
		dependencies.push_back(&colNodes[i]);
		
		predLogPNodes.emplace_back(&logPGraph, dependencies, [this, i]() {
			double logP = -INFINITY;
			
			int64_t groupId = sgToPtr->assignmentNodes[i]();
			
			// Alpha-below actually sum(alphaBelow for g <= groupId),
			// plus an increase as a function of position
			double alphaBelow = predGroupParams[0].alphaBelowNode();
			for(int64_t g = 1; g <= groupId; g++) {
				alphaBelow += predGroupParams[g].alphaBelowNode();
			}
			alphaBelow += i * alphaBelowSlopeNode() / (sgToPtr->size - 1);
			
			// Alpha-niche similarly a sum + increase
			double alphaNiche = predGroupParams[0].alphaNicheNode();
			for(int64_t g = 1; g <= groupId; g++) {
				alphaNiche += predGroupParams[g].alphaNicheNode();
			}
			alphaNiche += i * alphaNicheSlopeNode() / (sgToPtr->size - 1);
			
			// Alpha-above used as baseline
			double alphaAbove = alphaAboveNode();
			
			// pAlphas, beta all constant across predators;
			// pAlphaNiche = max(pAlphaBelow, pAlphaAbove) + additional
			VectorXd pAlpha(3);
			pAlpha[0] = pAlphaBelowNode();
			pAlpha[2] = pAlphaAboveNode();
			pAlpha[1] = max(pAlpha[0], pAlpha[2]) + pAlphaNicheNode();
			double pBeta = pBetaNode();
			
			if(!isPositive(pAlpha) || !isPositive(pBeta)) {
				return nan("");
			}
			
			int64_t nPrey = sgFromPtr->size;
			for(int64_t start = 0; start < nPrey; start++) {
				for(int64_t end = start; end <= nPrey; end++) {
					double logPOneNiche = 0.0;
					
					// Conditional probability of niche location
					VectorXi64 nVec(3);
					nVec << start, end - start, nPrey - end;
					
					VectorXd nicheAlphaVec(3);
					nicheAlphaVec << alphaBelow, alphaNiche, alphaAbove;
					if(!isPositive(nicheAlphaVec)) {
						return nan("");
					}
					
//					cerr << "nicheAlphaVec: " << nicheAlphaVec.transpose() << endl;
					assert(nVec.sum() == nPrey);
					logPOneNiche += dirichletMultinomialLogP(nicheAlphaVec, nVec);
					
					// Probability of links below, within, and above niche
					VectorXi64 predCol = colNodes[i]();
					assert(predCol.size() == nPrey);
					VectorXi64 sums(3);
					sums[0] = predCol.segment(0, nVec[0]).sum();
					sums[1] = predCol.segment(start, nVec[1]).sum();
					if(nVec[2] > 0) {
						sums[2] = predCol.segment(end, nVec[2]).sum();
					}
					else {
						sums[2] = 0;
					}
					assert(sums.sum() == predCol.sum());
					for(int64_t i = 0; i < 3; i++) {
						logPOneNiche += betaBernoulliLogP(
							pAlpha[i], pBeta, sums[i], nPrey - sums[i]
						);
					}
					
					if(std::isfinite(logPOneNiche)) {
						if(logP == -INFINITY) {
							logP = logPOneNiche;
						}
						else {
							logP = logSumExp(logP, logPOneNiche);
							assert(std::isfinite(logP));
						}
					}
				}
			}
			
			return logP;
		});
	}
}

void NetBlock::appendParams(std::vector<dagoberto::Value<double> *> & params)
{
	params.push_back(&alphaBelowSlopeNode);
	params.push_back(&alphaNicheSlopeNode);
	params.push_back(&alphaAboveNode);
	
	params.push_back(&pAlphaBelowNode);
	params.push_back(&pAlphaNicheNode);
	params.push_back(&pAlphaAboveNode);
	params.push_back(&pBetaNode);
	
	for(auto & pgp : predGroupParams) {
		pgp.appendParams(params);
	}
}

void NetBlock::swap(Supergroup *sg, int64_t i1, int64_t i2, bool updateNodes)
{
	if(sgFromPtr == sg) {
		data.row(i1).swap(data.row(i2));
	}
	if(sgToPtr == sg) {
		data.col(i1).swap(data.col(i2));
	}
	
	if(updateNodes) {
		if(sgFromPtr == sg) {
			for(int64_t i = 0; i < sgToPtr->size; i++) {
				colNodes[i] = data.col(i);
			}
		}
		else if(sgToPtr == sg) {
			colNodes[i1] = data.col(i1);
			colNodes[i2] = data.col(i2);
		}
	}
}

GroupNicheModel::GroupNicheModel(Config & config, zppdata::Database & db)
{
	_logPGraph.beginInitialization();
	
	vector<Node<double> *> logPTerms;
	
	// Construct supergroup objects
	unordered_map<string,int64_t> sgIndexMap;
	_supergroups.reserve(config.supergroups.size());
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		_supergroups.emplace_back(db,
			config.supergroups[i].id,
			config.supergroups[i].nGroups,
			i, _logPGraph
		);
		sgIndexMap[config.supergroups[i].id] = _supergroups.size() - 1;
	}
	
	// Construct block objects
	_blocks.reserve(config.blocks.size());
	for(auto & blockConfig : config.blocks) {
		_blocks.emplace_back(db,
			_supergroups[sgIndexMap[blockConfig.from]],
			_supergroups[sgIndexMap[blockConfig.to]],
			_logPGraph
		);
	}
	
	// Add block params/model and group params to log-p calculation
	for(auto & block : _blocks) {
		logPTerms.push_back(block.paramLogPNode.get());
		for(auto & predGroup : block.predGroupParams) {
			logPTerms.push_back(predGroup.paramLogPNode.get());
		}
		for(auto & predLogP : block.predLogPNodes) {
			logPTerms.push_back(&predLogP);
		}
	}
	
	_logPRoot = unique_ptr<VectorSum<double>>(
		new VectorSum<double>(&_logPGraph, logPTerms)
	);
	
	_logPGraph.endInitialization(_logPRoot.get());
	
	for(auto & block : _blocks) {
		block.appendParams(_params);
	}
}

void GroupNicheModel::verify()
{
	double oldLogP = logP();
	_logPGraph.recalculateAll();
	double newLogP = logP();
//	cerr << "old, new: " << oldLogP << ", " << newLogP << endl;
	assert(oldLogP == newLogP);
}

double GroupNicheModel::logP()
{
	return _logPRoot->evaluate();
}

Eigen::MatrixXd GroupNicheModel::covarianceGuess()
{
	return MatrixXd::Identity(_params.size(), _params.size());
}

Eigen::VectorXd GroupNicheModel::parameters()
{
	VectorXd paramValues(_params.size());
	for(int64_t i = 0; i < _params.size(); i++) {
		paramValues[i] = _params[i]->evaluate();
	}
	return paramValues;
}

void GroupNicheModel::propose(Eigen::VectorXd const & newParams)
{
	_logPGraph.beginTransaction();
	for(int64_t i = 0; i < _params.size(); i++) {
		*_params[i] = newParams[i];
	}
	_logPGraph.endTransaction();
}

void GroupNicheModel::accept(Eigen::VectorXd const & newParams)
{
	_logPGraph.commit();
}

void GroupNicheModel::reject(Eigen::VectorXd const & oldParams)
{
	_logPGraph.rollback();
}

void GroupNicheModel::iterateDiscrete(zppsim::rng_t & rng)
{
	for(int64_t sgId : randomPermutation(_supergroups.size(), rng)) {
		_iterateOrder(sgId, rng);
	}
	for(int64_t sgId : randomPermutation(_supergroups.size(), rng)) {
		_iterateGrouping(sgId, rng);
	}
}

void GroupNicheModel::_iterateOrder(int64_t sgId, zppsim::rng_t & rng)
{
	// Propose neighbor swap for each position in random order
	vector<int64_t> swapOrder = randomPermutation(_supergroups[sgId].size - 1, rng);
	for(int64_t i : swapOrder) {
		_proposeSwap(sgId, i, i+1, rng);
	}
	
	// Propose random long-distance swaps
	for(int64_t i = 0; i < _supergroups[sgId].size; i++) {
		int64_t i1 = drawUniformIndex(rng, _supergroups[sgId].size);
		int64_t i2 = drawUniformIndexExcept(rng, _supergroups[sgId].size, i1);
		_proposeSwap(sgId, i1, i2, rng);
	}
}

void GroupNicheModel::_proposeSwap(int64_t sgId, int64_t i1, int64_t i2, zppsim::rng_t & rng)
{
//	fprintf(stderr, "proposing neighbor swap %ld, %ld\n", sgId, index);
	
	Supergroup * sg = &_supergroups[sgId];
	
	double oldLogP = _logPRoot->evaluate();
	_logPGraph.beginTransaction();
	std::swap(sg->order[i1], sg->order[i2]);
	for(auto & block : _blocks) {
		block.swap(sg, i1, i2, true);
	}
	_logPGraph.endTransaction();
	double newLogP = _logPRoot->evaluate();
	
	if(shouldAcceptMHSymmetric(oldLogP, newLogP, rng)) {
		_logPGraph.commit();
	}
	else {
		std::swap(sg->order[i1], sg->order[i2]);
		for(auto & block : _blocks) {
			block.swap(sg, i1, i2, false);
		}
		_logPGraph.rollback();
	}
}

void GroupNicheModel::_iterateGrouping(int64_t sgId, zppsim::rng_t & rng)
{
	Supergroup * sg = &_supergroups[sgId];
	for(int64_t leftGroup : randomPermutation(sg->nGroups - 1, rng)) {
		int64_t divId = leftGroup + 1;
//		cerr << "proposing for group " << leftGroup << endl;
		int64_t leftDivLoc = sg->dividers[divId - 1];
		int64_t rightDivLoc = sg->dividers[divId + 1];
		
		for(int64_t newDivOffset : randomPermutation(rightDivLoc - leftDivLoc - 1, rng)) {
			int64_t oldDivLoc = sg->dividers[divId];
			int64_t newDivLoc = leftDivLoc + 1 + newDivOffset;
			
//			fprintf(stderr, "proposing group %ld, divider location %ld\n", leftGroup, newDivLoc);
			
			double oldLogP = _logPRoot->evaluate();
			_logPGraph.beginTransaction();
			
			sg->dividers[divId] = newDivLoc;
			VectorXi64 assignments = assignmentsFromDividers(sg->dividers);
			for(int64_t i = 0; i < sg->size; i++) {
				sg->assignmentNodes[i] = assignments[i];
			}
			
			_logPGraph.endTransaction();
			double newLogP = _logPRoot->evaluate();
//			cerr << "newLogP: " << newLogP << endl;
			if(shouldAcceptMHSymmetric(oldLogP, newLogP, rng)) {
//				cerr << "accepted: " << endl;
				_logPGraph.commit();
			}
			else {
				sg->dividers[divId] = oldDivLoc;
				_logPGraph.rollback();
			}
		}
	}
}

VectorXi64 GroupNicheModel::getSupergroupOrder(int64_t sgIndex)
{
	return _supergroups[sgIndex].order;
}

std::string GroupNicheModel::getSpeciesId(int64_t sgIndex, int64_t speciesIndex)
{
	return _supergroups[sgIndex].nodeIds[speciesIndex];
}

VectorXi64 GroupNicheModel::getSupergroupDividers(int64_t sgIndex)
{
	return _supergroups[sgIndex].dividers;
}

std::vector<std::pair<std::string, double>> GroupNicheModel::getBlockParams(int64_t blockIndex)
{
	vector<pair<string, double>> paramVec;
	paramVec.emplace_back("alphaBelowSlope", _blocks[blockIndex].alphaBelowSlopeNode());
	paramVec.emplace_back("alphaNicheSlope", _blocks[blockIndex].alphaNicheSlopeNode());
	paramVec.emplace_back("alphaAbove", _blocks[blockIndex].alphaAboveNode());
	paramVec.emplace_back("pAlphaBelow", _blocks[blockIndex].pAlphaBelowNode());
	paramVec.emplace_back("pAlphaNiche", _blocks[blockIndex].pAlphaNicheNode());
	paramVec.emplace_back("pAlphaAbove", _blocks[blockIndex].pAlphaAboveNode());
	paramVec.emplace_back("pBeta", _blocks[blockIndex].pBetaNode());
	return paramVec;
}

int64_t GroupNicheModel::fromGroupCount(int64_t blockIndex)
{
	return _blocks[blockIndex].sgToPtr->nGroups;
}

std::vector<std::pair<std::string, double>> GroupNicheModel::getBlockGroupParams(int64_t blockIndex, int64_t groupIndex)
{
	vector<pair<string, double>> paramVec;
	paramVec.emplace_back("alphaBelow", _blocks[blockIndex].predGroupParams[groupIndex].alphaBelowNode());
	paramVec.emplace_back("alphaNiche", _blocks[blockIndex].predGroupParams[groupIndex].alphaNicheNode());
	return paramVec;
}

} // namespace groupniche
