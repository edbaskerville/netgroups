#include "DCGroupModel.hpp"
#include "netgroups_util.hpp"
#include "zppsim_random.hpp"
#include <random>

using namespace std;
using namespace dagoberto;
using namespace Eigen;
using namespace zppdata;
using namespace zppsim;
using namespace boost;

namespace dcgroup
{

Supergroup::Supergroup(Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph) :
	id(supergroupId),
	nGroups(nGroups),
	index(index),
	nodeIds(loadSupergroup(db, supergroupId)),
	nodeIndexes(makeIndexMap(nodeIds)),
	size(nodeIds.size())
{
	// TODO: randomize initial conditions
	// For now: just divide species up roughly equally
	VectorXi64 assignments(size);
	vector<dynamic_bitset<>> groupSets;
	groupSets.reserve(nGroups);
	for(int64_t i = 0; i < nGroups; i++) {
		groupSets.emplace_back(size);
	}
	for(int64_t i = 0; i < size; i++) {
		assignments[i] = min(i * nGroups / size, nGroups - 1);
		groupSets[assignments[i]].set(i);
	}
	
	assignmentNodes.reserve(size);
	for(int64_t i = 0; i < size; i++) {
		assignmentNodes.emplace_back(&logPGraph, assignments[i]);
	}
	
	groupNodes.reserve(nGroups);
	for(int64_t g = 0; g < nGroups; g++) {
		groupNodes.emplace_back(&logPGraph, groupSets[g]);
	}
}

NetBlock::NetBlock(
	zppdata::Database & db,
	Supergroup & sgFrom, Supergroup & sgTo,
	dagoberto::Graph & logPGraph
) :
	sgFromPtr(&sgFrom), sgToPtr(&sgTo),
	data(loadNetwork(db, sgFrom.id, sgTo.id)),
	pAlphaLambdaNode(&logPGraph, 1.0),
	pBetaLambdaNode(&logPGraph, 1.0),
	pAlphaNodes(sgFrom.nGroups),
	pBetaNodes(sgFrom.nGroups),
	predPAlphaNodes(sgTo.size),
	predPBetaNodes(sgTo.size),
	predLinkLogPs(sgTo.size)
{
	cerr << data << endl << endl;
	
	// UPON RETURN:
	// * Fill in all nodes for link log-p (DONE)
	// * Make sure all parameter priors are in log-p (DONE)
	// * Write group proposals: should update both assignment and group sets
	// * Submit DC-group runs
	// * Review WAIC etc. & write code to extract it from outputs
	
	// pAlpha, pBeta for each pair of groups
	for(int64_t i = 0; i < sgFrom.nGroups; i++) {
		pAlphaNodes[i].reserve(sgTo.nGroups);
		pBetaNodes[i].reserve(sgTo.nGroups);
		for(int64_t j = 0; j < sgTo.nGroups; j++) {
			pAlphaNodes[i].emplace_back(&logPGraph, 1.0);
			pBetaNodes[i].emplace_back(&logPGraph, 1.0);
		}
	}
	pAlphaMatrixNode = unique_ptr<MatrixMultiplexer<double>>(
		new MatrixMultiplexer<double>(
			&logPGraph,
			pAlphaNodes
		)
	);
	pBetaMatrixNode = unique_ptr<MatrixMultiplexer<double>>(
		new MatrixMultiplexer<double>(
			&logPGraph,
			pBetaNodes
		)
	);
	
	// pAlpha, pBeta for each predator/prey group pair
	// link log-ps for each predator/prey group pair
	for(int64_t j = 0; j < sgTo.size; j++) {
		// Take pAlpha matrix, assignment to extract value for pAlpha, pBeta
		predPAlphaNodes[j].reserve(sgFrom.nGroups);
		predPBetaNodes[j].reserve(sgFrom.nGroups);
		predLinkLogPs[j].reserve(sgFrom.nGroups);
		for(int64_t gi = 0; gi < sgFrom.nGroups; gi++) {
			// Depends on pAlpha matrix and assignment node
			predPAlphaNodes[j].emplace_back(
				&logPGraph,
				(vector<NodeBase *>){pAlphaMatrixNode.get(), &sgTo.assignmentNodes[j]},
				[this, j, gi, &sgTo, &sgFrom]() {
					MatrixXd pAlphaMatrix = pAlphaMatrixNode->evaluate();
					return pAlphaMatrix(gi, sgTo.assignmentNodes[j]());
				}
			);
			
			// Depends on pBeta matrix and assignment node
			predPBetaNodes[j].emplace_back(
				&logPGraph,
				(vector<NodeBase *>){pBetaMatrixNode.get(), &sgTo.assignmentNodes[j]},
				[this, j, gi, &sgTo, &sgFrom]() {
					MatrixXd pBetaMatrix = pBetaMatrixNode->evaluate();
					return pBetaMatrix(gi, sgTo.assignmentNodes[j]());
				}
			);
			
			// Depends on corresponding predator pAlpha, pBeta
			// and prey group-membership nodes
			predLinkLogPs[j].emplace_back(
				&logPGraph,
				(vector<NodeBase *>){
					&predPAlphaNodes[j][gi], &predPBetaNodes[j][gi], &sgFrom.groupNodes[gi]
				},
				[this, j, gi, &sgFrom]() {
					
					double pAlpha = predPAlphaNodes[j][gi]();
					double pBeta = predPBetaNodes[j][gi]();
					
					if(!isPositive(pAlpha) || !isPositive(pBeta)) {
//						cerr << "returning nan" << endl;
						return nan("");
					}
					
					dynamic_bitset<> preyGroup = sgFrom.groupNodes[gi]();
					int64_t nLinks = getColLinkCount(preyGroup, j);
					int64_t nNonLinks = preyGroup.count() - nLinks;
					
					return betaBernoulliLogP(pAlpha, pBeta, nLinks, nNonLinks);
				}
			);
		}
	}
	
	// Log-p for parameters
	paramLogPNode = unique_ptr<Function<double>>(new Function<double>(
		&logPGraph,
		{&pAlphaLambdaNode, &pBetaLambdaNode, pAlphaMatrixNode.get(), pBetaMatrixNode.get()},
		[this, &sgFrom, &sgTo]() {
			double logP = 0.0;
			
//			cerr << "calculating param log p" << endl;
			
			double pAlphaLambda = pAlphaLambdaNode();
			double pBetaLambda = pBetaLambdaNode();
			
			if(!isPositive(pAlphaLambda) || !isPositive(pBetaLambda)) {
//				cerr << "returning nan" << endl;
				return nan("");
			}
			
			logP += exponentialLogP(1.0, pAlphaLambda);
			logP += exponentialLogP(1.0, pBetaLambda);
			
			MatrixXd pAlphaMatrix = pAlphaMatrixNode->evaluate();
			MatrixXd pBetaMatrix = pBetaMatrixNode->evaluate();
			for(int64_t gi = 0; gi < sgFrom.nGroups; gi++) {
				for(int64_t gj = 0; gj < sgTo.nGroups; gj++) {
					double pAlpha = pAlphaMatrix(gi, gj);
					double pBeta = pBetaMatrix(gi, gj);
					if(!isPositive(pAlpha) || !isPositive(pBeta)) {
//						cerr << "returning nan" << endl;
						return nan("");
					}
					
					logP += exponentialLogP(pAlphaLambda, pAlpha);
					logP += exponentialLogP(pBetaLambda, pBeta);
				}
			}
			
			return logP;
		}
	));
}

int64_t NetBlock::getColLinkCount(dynamic_bitset<> const & fromIndexes, int64_t toIndex)
{
	int64_t linkCount = 0;
	for(int64_t i = fromIndexes.find_first();
		i != dynamic_bitset<>::npos;
		i = fromIndexes.find_next(i)
	) {
		linkCount += data(i, toIndex);
	}
	return linkCount;
}

void NetBlock::appendParams(std::vector<dagoberto::Value<double> *> & params)
{
	cerr << "appending params" << endl;
	params.push_back(&pAlphaLambdaNode);
	params.push_back(&pBetaLambdaNode);
	
	for(int64_t gi = 0; gi < sgFromPtr->nGroups; gi++) {
		for(int64_t gj = 0; gj < sgToPtr->nGroups; gj++) {
			params.push_back(&pAlphaNodes[gi][gj]);
			params.push_back(&pBetaNodes[gi][gj]);
		}
	}
}

void NetBlock::appendLogPTerms(vector<Node<double> *> & logPTerms)
{
	logPTerms.push_back(paramLogPNode.get());
	for(int64_t j = 0; j < sgToPtr->size; j++) {
		for(int64_t gi = 0; gi < sgFromPtr->nGroups; gi++) {
			logPTerms.push_back(&predLinkLogPs[j][gi]);
		}
	}
}

DCGroupModel::DCGroupModel(Config & config, zppdata::Database & db)
{
	_logPGraph.beginInitialization();
	
	vector<Node<double> *> logPTerms;
	
	// Construct supergroup objects
	unordered_map<string,int64_t> sgIndexMap;
	_supergroups.reserve(config.supergroups.size());
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		_supergroups.emplace_back(db,
			config.supergroups[i].id,
			config.supergroups[i].nGroups,
			i, _logPGraph
		);
		sgIndexMap[config.supergroups[i].id] = _supergroups.size() - 1;
	}
	
	// Construct block objects
	_blocks.reserve(config.blocks.size());
	for(auto & blockConfig : config.blocks) {
		_blocks.emplace_back(db,
			_supergroups[sgIndexMap[blockConfig.from]],
			_supergroups[sgIndexMap[blockConfig.to]],
			_logPGraph
		);
	}
	
	// Add block params to log-p calculation
	for(auto & block : _blocks) {
		block.appendLogPTerms(logPTerms);
	}
	
	_logPRoot = unique_ptr<VectorSum<double>>(
		new VectorSum<double>(&_logPGraph, logPTerms)
	);
	
	_logPGraph.endInitialization(_logPRoot.get());
	
	for(auto & block : _blocks) {
		block.appendParams(_params);
	}
}

void DCGroupModel::verify()
{
	double oldLogP = logP();
	_logPGraph.recalculateAll();
	double newLogP = logP();
//	cerr << "old, new: " << oldLogP << ", " << newLogP << endl;
	assert(oldLogP == newLogP);
}

double DCGroupModel::logP()
{
	return _logPRoot->evaluate();
}

Eigen::MatrixXd DCGroupModel::covarianceGuess()
{
	return MatrixXd::Identity(_params.size(), _params.size());
}

Eigen::VectorXd DCGroupModel::parameters()
{
	VectorXd paramValues(_params.size());
	for(int64_t i = 0; i < _params.size(); i++) {
		paramValues[i] = _params[i]->evaluate();
	}
	return paramValues;
}

void DCGroupModel::propose(Eigen::VectorXd const & newParams)
{
	_logPGraph.beginTransaction();
	for(int64_t i = 0; i < _params.size(); i++) {
		*_params[i] = newParams[i];
	}
	_logPGraph.endTransaction();
}

void DCGroupModel::accept(Eigen::VectorXd const & newParams)
{
//	cerr << "committing " << _logPRoot->evaluate() << endl;
	_logPGraph.commit();
}

void DCGroupModel::reject(Eigen::VectorXd const & oldParams)
{
//	cerr << "rejecting " << _logPRoot->evaluate() << endl;
	_logPGraph.rollback();
}

void DCGroupModel::iterateDiscrete(zppsim::rng_t & rng)
{
	for(int64_t sgId : randomPermutation(_supergroups.size(), rng)) {
		_iterateGrouping(sgId, rng);
	}
}

void DCGroupModel::_iterateGrouping(int64_t sgId, zppsim::rng_t & rng)
{
	for(int64_t nodeIndex : randomPermutation(_supergroups[sgId].size, rng)) {
		if(_supergroups[sgId].nGroups > 1) {
			for(int64_t groupIndex : randomPermutation(_supergroups[sgId].nGroups, rng)) {
				_proposeGroupMove(sgId, nodeIndex, groupIndex, rng);
			}
		}
	}
}

void DCGroupModel::_proposeGroupMove(int64_t sgId, int64_t nodeIndex, int64_t newGroupIndex, zppsim::rng_t & rng)
{
//	fprintf(stderr, "proposing move %lld.%lld : g%lld\n", sgId, nodeIndex, newGroup);
	int64_t oldGroupIndex = _supergroups[sgId].assignmentNodes[nodeIndex]();
	dynamic_bitset<> oldGroup = _supergroups[sgId].groupNodes[oldGroupIndex]();
	dynamic_bitset<> newGroup = _supergroups[sgId].groupNodes[newGroupIndex]();
	
	if(oldGroupIndex ==  newGroupIndex) {
//		cerr << "new group same as old group" << endl;
		return;
	}
	
	if(oldGroup.count() == 1) {
//		cerr << "old group has only one member" << endl;
		return;
	}
	
	double oldLogP = _logPRoot->evaluate();
	_logPGraph.beginTransaction();
	
	_supergroups[sgId].assignmentNodes[nodeIndex] = newGroupIndex;
	
	oldGroup.set(nodeIndex, false);
	_supergroups[sgId].groupNodes[oldGroupIndex] = oldGroup;
	
	newGroup.set(nodeIndex, true);
	_supergroups[sgId].groupNodes[newGroupIndex] = newGroup;
	
	_logPGraph.endTransaction();
	
	double newLogP = _logPRoot->evaluate();
	
	// Accept or reject.
	// Rejection doesn't need to update link counts; rollback() handles that
	// automatically.
	if(shouldAcceptMHSymmetric(oldLogP, newLogP, rng)) {
		_logPGraph.commit();
	}
	else {
		_logPGraph.rollback();
	}
}

std::string DCGroupModel::getSpeciesId(int64_t sgIndex, int64_t speciesIndex)
{
	return _supergroups[sgIndex].nodeIds[speciesIndex];
}

int64_t DCGroupModel::speciesCount(int64_t sgIndex)
{
	return _supergroups[sgIndex].size;
}

VectorXi64 DCGroupModel::getAssignments(int64_t sgIndex)
{
	VectorXi64 assignments(_supergroups[sgIndex].size);
	for(int64_t i = 0; i < _supergroups[sgIndex].size; i++) {
		assignments[i] = _supergroups[sgIndex].assignmentNodes[i]();
	}
	return assignments;
}

std::vector<std::pair<std::string, double>> DCGroupModel::getBlockParams(int64_t blockIndex)
{
	vector<pair<string, double>> paramVec;
	paramVec.emplace_back("pAlphaLambda", _blocks[blockIndex].pAlphaLambdaNode());
	paramVec.emplace_back("pBetaLambda", _blocks[blockIndex].pBetaLambdaNode());
	return paramVec;
}

std::int64_t DCGroupModel::getPreyGroupCount(int64_t blockIndex)
{
	return _blocks[blockIndex].sgFromPtr->nGroups;
}

std::int64_t DCGroupModel::getPredGroupCount(int64_t blockIndex)
{
	return _blocks[blockIndex].sgToPtr->nGroups;
}

std::vector<std::pair<std::string, double>> DCGroupModel::getGroupPairParams(int64_t blockIndex, int64_t gi, int64_t gj)
{
	vector<pair<string, double>> paramVec;
	paramVec.emplace_back(
		"pAlpha",
		_blocks[blockIndex].pAlphaNodes[gi][gj]()
	);
	paramVec.emplace_back(
		"pBeta",
		_blocks[blockIndex].pBetaNodes[gi][gj]()
	);
	return paramVec;
}

} // namespace group
