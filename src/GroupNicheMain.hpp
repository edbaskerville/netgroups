//
//  Main.h
//  netgroups
//
//  Created by Ed Baskerville on 8/5/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__GroupNicheMain__
#define __netgroups__GroupNicheMain__

#include <string>
#include "Database.hpp"
#include "Config.hpp"
#include "GroupNicheModel.hpp"
#include "MCMC.hpp"
#include "zppsim_random.hpp"

class GroupNicheMain
{
public:
	GroupNicheMain(Config & config);
	void run();
private:
	void _writeOutput(int64_t iteration);
	void _writeLogP(int64_t iteration);
	void _writeOrderParams(int64_t iteration);
	void _writeGroupParams(int64_t iteration);
	void _writeBlockParams(int64_t iteration);
	void _writeBlockGroupParams(int64_t iteration);
	
	Config config;
	
	zppdata::Database dataDb;
	zppdata::Database outputDb;
	
	zppsim::rng_t rng;
	
	groupniche::GroupNicheModel model;
	MCMC mcmc;
	
	zppdata::DBTable logPTable;
	zppdata::DBTable tuningTable;
	zppdata::DBTable covarianceTable;
	zppdata::DBTable orderTable;
	zppdata::DBTable groupTable;
	zppdata::DBTable blockParamTable;
	zppdata::DBTable blockGroupParamTable;
};

#endif /* defined(__netgroups__GroupNicheMain__) */
