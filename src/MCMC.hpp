#ifndef __netgroups__MCMC__
#define __netgroups__MCMC__

#include <Eigen/Dense>
#include "zppsim_random.hpp"
#include "Model.hpp"
#include "MVNormalDistribution.hpp"


class MCMC
{
public:
	typedef std::function<void(int64_t index, Eigen::VectorXd const & sample)> SampleCallback;
	
	MCMC(Model * model, zppsim::rng_t * rng, int64_t thin);
	MCMC(Model * model, zppsim::rng_t * rng, int64_t thin, SampleCallback callback);
	double tune(int64_t nSamples);
	double tune(int64_t nSamples, SampleCallback callback);
	double run(int64_t nSamples);
	double run(int64_t nSamples, SampleCallback callback);
	void runDiscrete(int64_t nSamples);
	void runDiscrete(int64_t nSamples, SampleCallback callback);
	
	Eigen::MatrixXd proposalCovariance();
	double proposalMultiplier();
private:
	Model * _model;
	zppsim::rng_t * _rng;
	int64_t _thin;
	SampleCallback _callback;
	
	Eigen::MatrixXd _covariance;
	double _multiplier;
	
	double _targetRate;
	
	bool iterate(Eigen::VectorXd & params, double & logP, MVNormalDistribution & normDist);
};

#endif /* defined(__netgroups__MCMC__) */
