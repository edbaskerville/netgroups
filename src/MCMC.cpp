#include "MCMC.hpp"
#include "netgroups_util.hpp"
#include <iostream>

using namespace std;
using namespace Eigen;
using namespace zppsim;

MCMC::MCMC(Model * model, rng_t * rng, int64_t thin) :
	_model(model),
	_rng(rng),
	_thin(thin),
	_callback(nullptr),
	_covariance(model->covarianceGuess()),
	_multiplier(2.38/sqrt(_covariance.rows())),
	_targetRate(0.25)
{
	assert(_thin > 0);
	assert(_covariance.rows() == _covariance.cols());
}

MCMC::MCMC(Model * model, rng_t * rng, int64_t thin, SampleCallback callback) :
	_model(model),
	_rng(rng),
	_thin(thin),
	_callback(callback),
	_covariance(model->covarianceGuess()),
	_multiplier(2.38 / sqrt(_covariance.rows())),
	_targetRate(0.25)
{
	assert(_thin > 0);
	assert(_covariance.rows() == _covariance.cols());
}

double MCMC::tune(int64_t nSamples)
{
	return tune(nSamples, _callback);
}

double MCMC::tune(int64_t nSamples, SampleCallback callback)
{
	assert(nSamples > 0);
	
	LLT<MatrixXd> cholesky(_covariance);
	if(cholesky.info() == Eigen::NumericalIssue) {
//		_covariance = MatrixXd::Identity(_covariance.rows(), _covariance.rows());
//		cholesky = _covariance.llt();
//		assert(cholesky.info() == Eigen::Success);
		cerr << "numerical issue!" << endl;
	}
	
	MVNormalDistribution normDist(cholesky);
	int64_t nAccepted(0);
	
	VectorXd sample = _model->parameters();
	double logP = _model->logP();
	MatrixXd samples(sample.rows(), nSamples);
	assert(sample.rows() == _covariance.rows());
	for(int64_t i = 0; i < nSamples; i++) {
		for(int64_t j = 0; j < _thin; j++) {
			_model->iterateDiscrete(*_rng);
			if(iterate(sample, logP, normDist)) {
				nAccepted++;
			}
		}
		samples.col(i) = sample;
		if(callback != nullptr) {
			callback(i, sample);
		}
	}
	
	if(nAccepted > 0) {
		_covariance = unbiasedCovariance(samples);
	}
	
	double acceptanceRate = double(nAccepted) / double(nSamples * _thin);
	if(acceptanceRate == 0.0) {
		_multiplier /= 2;
	}
	else if(acceptanceRate == 1.00) {
		_multiplier *= 2;
	}
	else {
		_multiplier *= (1.0 - _targetRate) / (1.0 - acceptanceRate);
	}
	
	return acceptanceRate;
}

double MCMC::run(int64_t nSamples)
{
	return run(nSamples, _callback);
}

double MCMC::run(int64_t nSamples, SampleCallback callback)
{
	assert(nSamples > 0);
	
	MVNormalDistribution normDist(_covariance);
	int64_t nAccepted(0);
	
	VectorXd sample = _model->parameters();
	double logP = _model->logP();
	assert(sample.rows() == _covariance.rows());
	for(int64_t i = 0; i < nSamples; i++) {
		for(int64_t j = 0; j < _thin; j++) {
			_model->iterateDiscrete(*_rng);
			if(iterate(sample, logP, normDist)) {
				nAccepted++;
			}
		}
		if(callback != nullptr) {
			callback(i, sample);
		}
	}
	return double(nAccepted) / double(nSamples * _thin);
}

void MCMC::runDiscrete(int64_t nSamples)
{
	for(int64_t i = 0; i < nSamples; i++) {
		for(int64_t j = 0; j < _thin; j++) {
			_model->iterateDiscrete(*_rng);
		}
	}
}

void MCMC::runDiscrete(int64_t nSamples, SampleCallback callback)
{
	for(int64_t i = 0; i < nSamples; i++) {
		for(int64_t j = 0; j < _thin; j++) {
			_model->iterateDiscrete(*_rng);
		}
		VectorXd sample = _model->parameters();
		if(callback != nullptr) {
			callback(i, sample);
		}
	}
}

Eigen::MatrixXd MCMC::proposalCovariance()
{
	return _covariance;
}

double MCMC::proposalMultiplier()
{
	return _multiplier;
}

bool MCMC::iterate(Eigen::VectorXd & params, double & logP, MVNormalDistribution & normDist)
{
	Eigen::VectorXd newParams = params + normDist(*_rng) * _multiplier;
	_model->propose(newParams);
	double newLogP = _model->logP();
	
//	cerr << params.transpose() << endl;
	
	bool accepted;
	if(!std::isfinite(newLogP)) {
		accepted = false;
	}
	else if(newLogP > logP) {
		accepted = true;
	}
	else {
		bernoulli_distribution flipCoin(exp(newLogP - logP));
		accepted = flipCoin(*_rng);
	}
	
	if(accepted) {
		_model->accept(newParams);
		params = newParams;
		logP = newLogP;
	}
	else {
		_model->reject(params);
	}
	
	return accepted;
}
