//
//  Config.h
//  netgroups
//
//  Created by Ed Baskerville on 8/4/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__Config__
#define __netgroups__Config__

#include "PtreeObject.hpp"

class SupergroupConfig : public zppdata::PtreeObject
{
public:
	std::string id;
	int64_t nGroups;
	
	SupergroupConfig();
	void verify();
};

class BlockConfig : public zppdata::PtreeObject
{
public:
	std::string name;
	std::string from;
	std::string to;
	
	BlockConfig();
	void verify();
};

class Config : public zppdata::PtreeObject
{
public:
	std::string model;
	std::string dataFilename;
	std::string outputFilename;
	
	int64_t thin = 1;
	int64_t iterationsPerRound = 100;
	int64_t nDiscreteRounds = 20;
	int64_t nTuningRounds = 20;
	int64_t nMarkovRounds = 200;
	
	std::vector<SupergroupConfig> supergroups;
	std::vector<BlockConfig> blocks;
		Config();
	void verify();
};

Config loadConfig(std::string const & filename);

#endif /* defined(__netgroups__Config__) */
