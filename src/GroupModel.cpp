#include "GroupModel.hpp"
#include "netgroups_util.hpp"
#include "zppsim_random.hpp"
#include <random>

using namespace std;
using namespace dagoberto;
using namespace Eigen;
using namespace zppdata;
using namespace zppsim;

namespace group
{

Supergroup::Supergroup(Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph) :
	id(supergroupId),
	nGroups(nGroups),
	index(index),
	nodeIds(loadSupergroup(db, supergroupId)),
	nodeIndexes(makeIndexMap(nodeIds)),
	size(nodeIds.size()),
	assignments(size),
	groupSets(vector<unordered_set<int64_t>>(nGroups))
{
	// TODO: randomize initial conditions
	// For now: just divide species up roughly equally
	for(int64_t i = 0; i < size; i++) {
		assignments[i] = min(i * nGroups / size, nGroups - 1);
		groupSets[assignments[i]].insert(i);
	}
	
	groupSizeNodes.reserve(nGroups);
	for(int64_t i = 0; i < nGroups; i++) {
//		cerr << "Group size: " << groupSets[i].size() << endl;
		groupSizeNodes.emplace_back(&logPGraph, groupSets[i].size());
	}
}

GroupPair::GroupPair(
	dagoberto::Graph & logPGraph,
	dagoberto::Value<double> & pAlphaNode,
	dagoberto::Value<double> & pBetaNode,
	Supergroup & sgFrom, int64_t groupFrom,
	Supergroup & sgTo, int64_t groupTo,
	int64_t nLinks
) :
	nLinksNode(&logPGraph, nLinks),
	logPNode(
		&logPGraph,
		{
			&pAlphaNode, &pBetaNode,
			&sgFrom.groupSizeNodes[groupFrom],
			&sgTo.groupSizeNodes[groupTo],
			&nLinksNode
		},
		[&pAlphaNode, &pBetaNode, &sgFrom, &sgTo, groupFrom, groupTo, this]() {
			double pAlpha = pAlphaNode();
			double pBeta = pBetaNode();
			if(!isPositive(pAlpha) || !isPositive(pBeta)) {
				return nan("");
			}
			
			int64_t nLinks = nLinksNode();
			assert(nLinks >= 0);
			int64_t fromSize = sgFrom.groupSizeNodes[groupFrom]();
			assert(fromSize > 0);
			int64_t toSize = sgTo.groupSizeNodes[groupTo]();
			assert(toSize > 0);
			int64_t nPossible = fromSize * toSize;
			int64_t nNonLinks = nPossible - nLinks;
			assert(nNonLinks >= 0);
			
			return betaBernoulliLogP(pAlpha, pBeta, nLinks, nNonLinks);
		}
	)
{
}

NetBlock::NetBlock(
	zppdata::Database & db,
	Supergroup & sgFrom, Supergroup & sgTo,
	dagoberto::Graph & logPGraph
) :
	sgFromPtr(&sgFrom), sgToPtr(&sgTo),
	data(loadNetwork(db, sgFrom.id, sgTo.id)),
	pAlphaNode(&logPGraph, 1.0),
	pBetaNode(&logPGraph, 1.0)
{
	cerr << data << endl << endl;
	
	// Log-p for parameters
	paramLogPNode = unique_ptr<Function<double>>(new Function<double>(
		&logPGraph,
		{&pAlphaNode, &pBetaNode},
		[this]() {
			double logP = 0.0;
			
			logP += exponentialLogP(1.0, pAlphaNode());
			logP += exponentialLogP(1.0, pBetaNode());
			
			return logP;
		}
	));
	
	// Group pairs
	groupPairs.reserve(sgFromPtr->nGroups);
	for(int64_t i = 0; i < sgFromPtr->nGroups; i++) {
		groupPairs.push_back(std::vector<GroupPair>());
		groupPairs[i].reserve(sgToPtr->nGroups);
		
		for(int64_t j = 0; j < sgToPtr->nGroups; j++) {
			groupPairs[i].emplace_back(
				logPGraph,
				pAlphaNode,
				pBetaNode,
				sgFrom, i,
				sgTo, j,
				getLinkCount(
					sgFrom.groupSets[i],
					sgTo.groupSets[j]
				)
			);
		}
	}
}

int64_t NetBlock::getLinkCount(std::unordered_set<int64_t> const & fromIndexes, std::unordered_set<int64_t> const & toIndexes)
{
	int64_t linkCount = 0;
	for(int64_t i : fromIndexes) {
		for(int64_t j : toIndexes) {
			linkCount += data(i,j);
		}
	}
	return linkCount;
}

int64_t NetBlock::getRowLinkCount(int64_t fromIndex, std::unordered_set<int64_t> const & toIndexes, bool excludeSelfLinks)
{
	int64_t linkCount = 0;
	for(int64_t i : toIndexes) {
		if(!excludeSelfLinks || (sgFromPtr != sgToPtr) || (i != fromIndex)) {
			linkCount += data(fromIndex, i);
		}
	}
	return linkCount;
}

int64_t NetBlock::getColLinkCount(std::unordered_set<int64_t> const & fromIndexes, int64_t toIndex, bool excludeSelfLinks)
{
	int64_t linkCount = 0;
	for(int64_t i : fromIndexes) {
		if(!excludeSelfLinks || (sgFromPtr != sgToPtr) || (i != toIndex)) {
			linkCount += data(i, toIndex);
		}
	}
	return linkCount;
}

void NetBlock::appendParams(std::vector<dagoberto::Value<double> *> & params)
{
	params.push_back(&pAlphaNode);
	params.push_back(&pBetaNode);
}

GroupModel::GroupModel(Config & config, zppdata::Database & db)
{
	_logPGraph.beginInitialization();
	
	vector<Node<double> *> logPTerms;
	
	// Construct supergroup objects
	unordered_map<string,int64_t> sgIndexMap;
	_supergroups.reserve(config.supergroups.size());
	for(int64_t i = 0; i < config.supergroups.size(); i++) {
		_supergroups.emplace_back(db,
			config.supergroups[i].id,
			config.supergroups[i].nGroups,
			i, _logPGraph
		);
		sgIndexMap[config.supergroups[i].id] = _supergroups.size() - 1;
	}
	
	// Construct block objects
	_blocks.reserve(config.blocks.size());
	for(auto & blockConfig : config.blocks) {
		_blocks.emplace_back(db,
			_supergroups[sgIndexMap[blockConfig.from]],
			_supergroups[sgIndexMap[blockConfig.to]],
			_logPGraph
		);
	}
	
	// Add block params to log-p calculation
	for(auto & block : _blocks) {
		logPTerms.push_back(block.paramLogPNode.get());
		for(auto & gpRow : block.groupPairs) {
			for(auto & gp : gpRow) {
				logPTerms.push_back(&gp.logPNode);
			}
		}
	}
	
	_logPRoot = unique_ptr<VectorSum<double>>(
		new VectorSum<double>(&_logPGraph, logPTerms)
	);
	
	_logPGraph.endInitialization(_logPRoot.get());
	
	for(auto & block : _blocks) {
		block.appendParams(_params);
	}
}

void GroupModel::verify()
{
	double oldLogP = logP();
	_logPGraph.recalculateAll();
	double newLogP = logP();
//	cerr << "old, new: " << oldLogP << ", " << newLogP << endl;
	assert(oldLogP == newLogP);
}

double GroupModel::logP()
{
	return _logPRoot->evaluate();
}

Eigen::MatrixXd GroupModel::covarianceGuess()
{
	return MatrixXd::Identity(_params.size(), _params.size());
}

Eigen::VectorXd GroupModel::parameters()
{
	VectorXd paramValues(_params.size());
	for(int64_t i = 0; i < _params.size(); i++) {
		paramValues[i] = _params[i]->evaluate();
	}
	return paramValues;
}

void GroupModel::propose(Eigen::VectorXd const & newParams)
{
	_logPGraph.beginTransaction();
	for(int64_t i = 0; i < _params.size(); i++) {
		*_params[i] = newParams[i];
	}
	_logPGraph.endTransaction();
}

void GroupModel::accept(Eigen::VectorXd const & newParams)
{
	_logPGraph.commit();
}

void GroupModel::reject(Eigen::VectorXd const & oldParams)
{
	_logPGraph.rollback();
}

void GroupModel::iterateDiscrete(zppsim::rng_t & rng)
{
	for(int64_t sgId : randomPermutation(_supergroups.size(), rng)) {
		_iterateGrouping(sgId, rng);
	}
}

void GroupModel::_iterateGrouping(int64_t sgId, zppsim::rng_t & rng)
{
	for(int64_t nodeIndex : randomPermutation(_supergroups[sgId].size, rng)) {
		if(_supergroups[sgId].nGroups > 1) {
			for(int64_t groupIndex : randomPermutation(_supergroups[sgId].nGroups, rng)) {
				_proposeGroupMove(sgId, nodeIndex, groupIndex, rng);
			}
		}
	}
}

void GroupModel::_proposeGroupMove(int64_t sgId, int64_t nodeIndex, int64_t newGroup, zppsim::rng_t & rng)
{
//	fprintf(stderr, "proposing move %lld.%lld : g%lld\n", sgId, nodeIndex, newGroup);
	int64_t oldGroup = _supergroups[sgId].assignments[nodeIndex];
	
	if(oldGroup ==  newGroup) {
//		cerr << "new group same as old group" << endl;
		return;
	}
	
	if(_supergroups[sgId].groupSets[oldGroup].size() == 1) {
//		cerr << "old group has only one member" << endl;
		return;
	}
	
	double oldLogP = _logPRoot->evaluate();
	_logPGraph.beginTransaction();
	
	// Update group assignment and set membership
	_supergroups[sgId].assignments[nodeIndex] = newGroup;
	_supergroups[sgId].groupSets[oldGroup].erase(nodeIndex);
	_supergroups[sgId].groupSizeNodes[oldGroup] = _supergroups[sgId].groupSets[oldGroup].size();
	
	assert(_supergroups[sgId].groupSets[oldGroup].size() >= 1);
	_supergroups[sgId].groupSets[newGroup].insert(nodeIndex);
	_supergroups[sgId].groupSizeNodes[newGroup] = _supergroups[sgId].groupSets[newGroup].size();
	
	for(auto & block : _blocks) {
		int64_t preySG = block.sgFromPtr->index;
		int64_t predSG = block.sgToPtr->index;
		
		// Case 0: species in block's prey and predator group
		if(sgId == preySG || sgId == predSG) {
			// Get old link counts to be updated
			MatrixXi64 linkCounts(block.sgFromPtr->nGroups, block.sgToPtr->nGroups);
			for(int64_t preyG = 0; preyG < block.sgFromPtr->nGroups; preyG++) {
				for(int64_t predG = 0; predG < block.sgToPtr->nGroups; predG++) {
					linkCounts(preyG, predG) = block.groupPairs[preyG][predG].nLinksNode();
				}
			}
			
			// Case 1: species is in the block's prey group
			if(sgId == preySG) {
				for(int64_t predG = 0; predG < block.sgToPtr->nGroups; predG++) {
					// Update number of links in old/new group pairs
					// nLinkDelta will exclude self-links
					int64_t nLinkDelta = block.getRowLinkCount(
						nodeIndex, block.sgToPtr->groupSets[predG], true
					);
					linkCounts(oldGroup, predG) -= nLinkDelta;
					linkCounts(newGroup, predG) += nLinkDelta;
				}
			}
			
			// Case 2: species is in the block's predator group
			if(sgId == predSG) {
				for(int64_t preyG = 0; preyG < block.sgFromPtr->nGroups; preyG++) {
					// Update number of links in old/new group pairs
					// nLinkDelta will exclude self-links
					int64_t nLinkDelta = block.getColLinkCount(
						block.sgFromPtr->groupSets[preyG], nodeIndex, true
					);
					linkCounts(preyG, oldGroup) -= nLinkDelta;
					linkCounts(preyG, newGroup) += nLinkDelta;
				}
			}
			
			// Handle self-links
			if(sgId == preySG && sgId == predSG) {
				linkCounts(oldGroup, oldGroup) -= block.data(nodeIndex, nodeIndex);
				linkCounts(newGroup, newGroup) += block.data(nodeIndex, nodeIndex);
			}
	
			// Set new link counts
			for(int64_t preyG = 0; preyG < block.sgFromPtr->nGroups; preyG++) {
				for(int64_t predG = 0; predG < block.sgToPtr->nGroups; predG++) {
					block.groupPairs[preyG][predG].nLinksNode = linkCounts(preyG, predG);
				}
			}
		}
	}
	
	_logPGraph.endTransaction();
	
	/*for(auto & block : _blocks) {
		for(int i = 0; i < block.sgFromPtr->nGroups; i++) {
			for(int j = 0; j < block.sgToPtr->nGroups; j++) {
				int64_t linkCountFromScratch = block.getLinkCount(
					block.sgFromPtr->groupSets[i],
					block.sgToPtr->groupSets[j]
				);
				int64_t storedLinkCount = block.groupPairs[i][j].nLinksNode();
				assert(linkCountFromScratch == storedLinkCount);
			}
		}
	}*/
	
	double newLogP = _logPRoot->evaluate();
	
	// Accept or reject.
	// Rejection doesn't need to update link counts; rollback() handles that
	// automatically.
	if(shouldAcceptMHSymmetric(oldLogP, newLogP, rng)) {
		_logPGraph.commit();
	}
	else {
		_supergroups[sgId].assignments[nodeIndex] = oldGroup;
		_supergroups[sgId].groupSets[oldGroup].insert(nodeIndex);
		_supergroups[sgId].groupSets[newGroup].erase(nodeIndex);
		assert(_supergroups[sgId].groupSets[newGroup].size() >= 1);
		
		_logPGraph.rollback();
	}
}

std::string GroupModel::getSpeciesId(int64_t sgIndex, int64_t speciesIndex)
{
	return _supergroups[sgIndex].nodeIds[speciesIndex];
}

int64_t GroupModel::speciesCount(int64_t sgIndex)
{
	return _supergroups[sgIndex].size;
}

VectorXi64 GroupModel::getAssignments(int64_t sgIndex)
{
	return _supergroups[sgIndex].assignments;
}

std::vector<std::pair<std::string, double>> GroupModel::getBlockParams(int64_t blockIndex)
{
	vector<pair<string, double>> paramVec;
	paramVec.emplace_back("pAlpha", _blocks[blockIndex].pAlphaNode());
	paramVec.emplace_back("pBeta", _blocks[blockIndex].pBetaNode());
	return paramVec;
}

} // namespace group
