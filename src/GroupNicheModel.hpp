//
//  GroupNicheModel.h
//  netgroups
//
//  Created by Ed Baskerville on 8/4/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__GroupNicheModel__
#define __netgroups__GroupNicheModel__

#include <vector>
#include <Eigen/Dense>
#include "dagoberto.hpp"
#include "Model.hpp"
#include "Config.hpp"
#include "Database.hpp"
#include "netgroups_util.hpp"
#include <memory>

namespace groupniche
{

struct Supergroup
{
	Supergroup(zppdata::Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph);
	
	std::string id;
	int64_t nGroups;
	int64_t index;
	std::vector<std::string> nodeIds;
	int64_t size;
	std::unordered_map<std::string, int64_t> nodeIndexes;
	
	VectorXi64 order;
	VectorXi64 dividers;
	
	std::vector<dagoberto::Value<int64_t>> assignmentNodes;
};

struct PredGroupParams
{
	PredGroupParams(int64_t index, dagoberto::Graph & logPGraph);
	
	void appendParams(std::vector<dagoberto::Value<double> *> & params);
	
	int64_t index;
	
	// Dirichlet-multinomial parameters for
	// number of species below, within, and above predator niche
	dagoberto::Value<double> alphaBelowNode;
	dagoberto::Value<double> alphaNicheNode;
	
	std::unique_ptr<dagoberto::Function<double>> paramLogPNode;
};

struct NetBlock
{
	NetBlock(zppdata::Database & db, Supergroup & sgFrom, Supergroup & sgTo, dagoberto::Graph & logPGraph);
	
	void appendParams(std::vector<dagoberto::Value<double> *> & params);
	void swap(Supergroup * sg, int64_t i1, int64_t i2, bool updateNodes);
	
	Supergroup * sgFromPtr;
	Supergroup * sgToPtr;
	
	MatrixXi64 data; // Rows, cols kept reordered
	std::vector<dagoberto::Value<VectorXi64>> colNodes; // Mirrors data
	
	dagoberto::Value<double> alphaBelowSlopeNode;
	dagoberto::Value<double> alphaNicheSlopeNode;
	dagoberto::Value<double> alphaAboveNode;
	
	dagoberto::Value<double> pAlphaBelowNode;
	dagoberto::Value<double> pAlphaNicheNode;
	dagoberto::Value<double> pAlphaAboveNode;
	dagoberto::Value<double> pBetaNode;
	
	std::unique_ptr<dagoberto::Function<double>> paramLogPNode;
	
	std::vector<PredGroupParams> predGroupParams;
	
	std::vector<dagoberto::Function<double>> predLogPNodes;
};

class GroupNicheModel : public Model
{
public:
	GroupNicheModel(Config & config, zppdata::Database & db);
	
	void verify();
	
	virtual Eigen::VectorXd parameters();
	virtual double logP();
	
	virtual Eigen::MatrixXd covarianceGuess();
	
	virtual void propose(Eigen::VectorXd const & newParams);
	virtual void accept(Eigen::VectorXd const & newParams);
	virtual void reject(Eigen::VectorXd const & oldParams);
	
	virtual void iterateDiscrete(zppsim::rng_t & rng);
	
	VectorXi64 getSupergroupOrder(int64_t sgIndex);
	std::string getSpeciesId(int64_t sgIndex, int64_t speciesIndex);
	VectorXi64 getSupergroupDividers(int64_t sgIndex);
	
	std::vector<std::pair<std::string, double>> getBlockParams(int64_t blockIndex);
	int64_t fromGroupCount(int64_t blockIndex);
	std::vector<std::pair<std::string, double>> getBlockGroupParams(int64_t blockIndex, int64_t groupIndex);
	
private:
	void _iterateOrder(int64_t sgId, zppsim::rng_t & rng);
	void _proposeSwap(int64_t sgId, int64_t i1, int64_t i2, zppsim::rng_t & rng);
	void _iterateGrouping(int64_t sgId, zppsim::rng_t & rng);
	
	dagoberto::Graph _logPGraph;
	std::vector<Supergroup> _supergroups;
	std::vector<NetBlock> _blocks;
	
	std::vector<dagoberto::Value<double> *> _params;
	
	std::unique_ptr<dagoberto::VectorSum<double>> _logPRoot;
};

} // namespace groupniche

#endif /* defined(__netgroups__GroupNicheModel__) */
