#ifndef __netgroups__DCGroupMain__
#define __netgroups__DCGroupMain__

#include <string>
#include "Database.hpp"
#include "Config.hpp"
#include "DCGroupModel.hpp"
#include "MCMC.hpp"
#include "zppsim_random.hpp"

class DCGroupMain
{
public:
	DCGroupMain(Config & config);
	void run();
private:
	void _writeOutput(int64_t iteration);
	void _writeLogP(int64_t iteration);
	void _writeGroupParams(int64_t iteration);
	void _writeBlockParams(int64_t iteration);
	void _writeGroupPairParams(int64_t iteration);
	
	Config config;
	
	zppdata::Database dataDb;
	zppdata::Database outputDb;
	
	zppsim::rng_t rng;
	
	dcgroup::DCGroupModel model;
	MCMC mcmc;
	
	zppdata::DBTable logPTable;
	zppdata::DBTable tuningTable;
	zppdata::DBTable covarianceTable;
	zppdata::DBTable groupTable;
	zppdata::DBTable blockParamTable;
	zppdata::DBTable groupPairParamTable;
};

#endif /* defined(__netgroups__DCGroupMain__) */
