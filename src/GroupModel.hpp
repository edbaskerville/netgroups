#ifndef __netgroups__GroupModel__
#define __netgroups__GroupModel__

#include <vector>
#include <Eigen/Dense>
#include "dagoberto.hpp"
#include "Model.hpp"
#include "Config.hpp"
#include "Database.hpp"
#include "netgroups_util.hpp"
#include <unordered_set>
#include <memory>

namespace group
{

struct Supergroup
{
	Supergroup(zppdata::Database & db, std::string supergroupId, int64_t nGroups, int64_t index, dagoberto::Graph & logPGraph);
	
	std::string id;
	int64_t nGroups;
	int64_t index;
	std::vector<std::string> nodeIds;
	int64_t size;
	std::unordered_map<std::string, int64_t> nodeIndexes;
	
	VectorXi64 assignments;
	std::vector<std::unordered_set<int64_t>> groupSets;
	
	std::vector<dagoberto::Value<int64_t>> groupSizeNodes;
};

struct GroupPair
{
	GroupPair(
		dagoberto::Graph & logPGraph,
		dagoberto::Value<double> & pAlphaNode,
		dagoberto::Value<double> & pBetaNode,
		Supergroup & sgFrom, int64_t groupFrom,
		Supergroup & sgTo, int64_t groupTo,
		int64_t nLinks
	);
	
	dagoberto::Value<int64_t> fromSizeNode;
	dagoberto::Value<int64_t> toSizeNode;
	dagoberto::Value<int64_t> nLinksNode;
	
	dagoberto::Function<double> logPNode;
};

struct NetBlock
{
	NetBlock(zppdata::Database & db, Supergroup & sgFrom, Supergroup & sgTo, dagoberto::Graph & logPGraph);
	
	void appendParams(std::vector<dagoberto::Value<double> *> & params);
	int64_t getLinkCount(
		std::unordered_set<int64_t> const & fromIndexes,
		std::unordered_set<int64_t> const & toIndexes
	);
	
	int64_t getRowLinkCount(
		int64_t fromIndex,
		std::unordered_set<int64_t> const & toIndexes,
		bool excludeSelfLinks
	);
	
	int64_t getColLinkCount(
		std::unordered_set<int64_t> const & fromIndexes,
		int64_t toIndex,
		bool excludeSelfLinks
	);
	
	Supergroup * sgFromPtr;
	Supergroup * sgToPtr;
	
	MatrixXi64 data;
	
	dagoberto::Value<double> pAlphaNode;
	dagoberto::Value<double> pBetaNode;
	
	std::unique_ptr<dagoberto::Function<double>> paramLogPNode;
	
	std::vector<std::vector<GroupPair>> groupPairs;
};

class GroupModel : public Model
{
public:
	GroupModel(Config & config, zppdata::Database & db);
	
	void verify();
	
	virtual Eigen::VectorXd parameters();
	virtual double logP();
	
	virtual Eigen::MatrixXd covarianceGuess();
	
	virtual void propose(Eigen::VectorXd const & newParams);
	virtual void accept(Eigen::VectorXd const & newParams);
	virtual void reject(Eigen::VectorXd const & oldParams);
	
	virtual void iterateDiscrete(zppsim::rng_t & rng);
	
	std::string getSpeciesId(int64_t sgIndex, int64_t speciesIndex);
	int64_t speciesCount(int64_t sgIndex);
	VectorXi64 getAssignments(int64_t sgIndex);
	
	std::vector<std::pair<std::string, double>> getBlockParams(int64_t blockIndex);
	
private:
	void _iterateGrouping(int64_t sgId, zppsim::rng_t & rng);
	void _proposeGroupMove(int64_t sgId, int64_t nodeIndex, int64_t groupIndex, zppsim::rng_t & rng);
	
	dagoberto::Graph _logPGraph;
	std::vector<Supergroup> _supergroups;
	std::vector<NetBlock> _blocks;
	
	std::vector<dagoberto::Value<double> *> _params;
	
	std::unique_ptr<dagoberto::VectorSum<double>> _logPRoot;
};

} // namespace group

#endif /* defined(__netgroups__GroupModel__) */
