//
//  Main.h
//  netgroups
//
//  Created by Ed Baskerville on 8/5/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__GroupMain__
#define __netgroups__GroupMain__

#include <string>
#include "Database.hpp"
#include "Config.hpp"
#include "GroupModel.hpp"
#include "MCMC.hpp"
#include "zppsim_random.hpp"

class GroupMain
{
public:
	GroupMain(Config & config);
	void run();
private:
	void _writeOutput(int64_t iteration);
	void _writeLogP(int64_t iteration);
	void _writeGroupParams(int64_t iteration);
	void _writeBlockParams(int64_t iteration);
	
	Config config;
	
	zppdata::Database dataDb;
	zppdata::Database outputDb;
	
	zppsim::rng_t rng;
	
	group::GroupModel model;
	MCMC mcmc;
	
	zppdata::DBTable logPTable;
	zppdata::DBTable tuningTable;
	zppdata::DBTable covarianceTable;
	zppdata::DBTable groupTable;
	zppdata::DBTable blockParamTable;
};

#endif /* defined(__netgroups__GroupMain__) */
