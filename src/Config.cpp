//
//  Config.cpp
//  netgroups
//
//  Created by Ed Baskerville on 8/4/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "Config.hpp"
#include <fstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
using namespace boost;
using namespace boost::property_tree;

#define DEFINE_SETTING(x) define(#x, x)

Config loadConfig(std::string const & filename)
{
	ifstream configStream(filename);
	ptree configPtree;
	json_parser::read_json(configStream, configPtree);
	Config config;
	config.loadFromPtree(configPtree);
	return config;
}

SupergroupConfig::SupergroupConfig()
{
	DEFINE_SETTING(id);
	DEFINE_SETTING(nGroups);
}

void SupergroupConfig::verify()
{
	assert(id != "");
	assert(nGroups > 0);
}

BlockConfig::BlockConfig()
{
	DEFINE_SETTING(name);
	DEFINE_SETTING(from);
	DEFINE_SETTING(to);
}

void BlockConfig::verify()
{
	assert(name != "");
	assert(from != "");
	assert(to != "");
}

Config::Config()
{
	DEFINE_SETTING(model);
	
	DEFINE_SETTING(dataFilename);
	DEFINE_SETTING(outputFilename);
	
	DEFINE_SETTING(thin);
	DEFINE_SETTING(iterationsPerRound);
	DEFINE_SETTING(nDiscreteRounds);
	DEFINE_SETTING(nTuningRounds);
	DEFINE_SETTING(nMarkovRounds);
	
	DEFINE_SETTING(supergroups);
	DEFINE_SETTING(blocks);
}

void Config::verify()
{
	assert(model == "niche" || model == "group");
	
	assert(dataFilename != "");
	assert(outputFilename != "");
	
	assert(thin > 0);
	assert(iterationsPerRound > 0);
	assert(nTuningRounds >= 0);
	assert(nMarkovRounds >= 0);
	
	for(auto & supergroup : supergroups) {
		supergroup.verify();
	}
	for(auto & block : blocks) {
		block.verify();
	}
}
