//
//  MVNormalProposer.h
//  netgroups
//
//  Created by Ed Baskerville on 7/31/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__util__
#define __netgroups__util__

#include <Eigen/Dense>
#include "Database.hpp"
#include "zppsim_random.hpp"

typedef Eigen::Matrix<int64_t, Eigen::Dynamic, 1> VectorXi64;
typedef Eigen::Matrix<int64_t, Eigen::Dynamic, Eigen::Dynamic> MatrixXi64;

Eigen::MatrixXd unbiasedCovariance(Eigen::MatrixXd X);

double normalizedEntryError(Eigen::MatrixXd xCorrect, Eigen::MatrixXd xWrong);

Eigen::MatrixXd logMatrix(Eigen::MatrixXd const & x);
Eigen::VectorXd logVector(Eigen::VectorXd const & x);
Eigen::MatrixXd expMatrix(Eigen::MatrixXd const & x);
Eigen::VectorXd expVector(Eigen::VectorXd const & x);
double logSumExp(double a, double b);
double lbeta(double x, double y);

Eigen::MatrixXd permuteMatrix(Eigen::MatrixXd const & X, VectorXi64 const & indexOrder);
MatrixXi64 permuteMatrix(MatrixXi64 const & X, VectorXi64 const & indexOrder);

double exponentialLogP(double const & lambda, double const & x);
double exponentialLogPVector(double const & lambda, Eigen::VectorXd const & x);

double dirichletMultinomialLogP(Eigen::VectorXd const & alphaVec, VectorXi64 & nVec);
double betaBernoulliLogP(double alpha, double beta, int64_t nSuccesses, int64_t nFailures);

template<typename T>
std::unordered_map<T, int64_t> makeIndexMap(std::vector<T> const & vec)
{
	std::unordered_map<T, int64_t> indexMap;
	for(int64_t i = 0; i < vec.size(); i++) {
		indexMap[vec[i]] = i;
	}
	return indexMap;
}

VectorXi64 assignmentsFromDividers(VectorXi64 const & dividers);

std::vector<std::string> loadSupergroup(zppdata::Database & db, std::string const & supergroup);

MatrixXi64 loadNetwork(zppdata::Database & db, std::string const & fromSupergroup, std::string const & toSupergroup);

bool isPositive(double x);

bool isPositive(Eigen::VectorXd x);

bool shouldAcceptMHSymmetric(double oldLogP, double newLogP, zppsim::rng_t & rng);

std::vector<int64_t> randomPermutation(int64_t size, zppsim::rng_t & rng);

#endif /* defined(__netgroups__util__) */
