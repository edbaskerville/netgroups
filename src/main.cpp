//
//  main.cpp
//  netgroups
//
//  Created by Ed Baskerville on 8/5/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include <cassert>
#include "DCGroupMain.hpp"
#include "GroupMain.hpp"
#include "GroupNicheMain.hpp"
#include <iostream>

using namespace std;

int main(int argc, char const * const * const argv)
{
	assert(argc == 2);
	Config config(loadConfig(argv[1]));
	if(config.model == "groupniche") {
		GroupNicheMain mainObj(config);
		mainObj.run();
	}
	else if(config.model == "group") {
		GroupMain mainObj(config);
		mainObj.run();
	}
	else if(config.model == "dcgroup") {
		DCGroupMain mainObj(config);
		mainObj.run();
	}
	else {
		cerr << "Invalid model specified. Quitting." << endl;
	}
}
