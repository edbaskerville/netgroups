//
//  MVNormalDistribution.h
//  netgroups
//
//  Created by Ed Baskerville on 8/2/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __netgroups__MVNormalDistribution__
#define __netgroups__MVNormalDistribution__

#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <random>

class MVNormalDistribution
{
public:
	MVNormalDistribution(Eigen::MatrixXd const & covariance) :
		_mean(Eigen::VectorXd::Zero(covariance.rows())),
		_cholesky(covariance),
		_L(_cholesky.matrixL())
	{
		assert(covariance.rows() == covariance.cols());
//		assert(_cholesky.info() != Eigen::NumericalIssue);
	}
	
	MVNormalDistribution(Eigen::LLT<Eigen::MatrixXd> const & cholesky) :
		_mean(Eigen::VectorXd::Zero(cholesky.matrixL().rows())),
		_cholesky(cholesky),
		_L(_cholesky.matrixL())
	{
//		assert(_cholesky.info() != Eigen::NumericalIssue);
	}
	
	MVNormalDistribution(Eigen::VectorXd mean, Eigen::MatrixXd covariance) :
		_mean(mean), _cholesky(covariance)
	{
		assert(mean.rows() == covariance.rows());
		assert(covariance.rows() == covariance.cols());
//		assert(_cholesky.info() != Eigen::NumericalIssue);
	}
	
	MVNormalDistribution(Eigen::VectorXd mean, Eigen::LLT<Eigen::MatrixXd> const & cholesky) :
		_mean(mean),
		_cholesky(cholesky),
		_L(_cholesky.matrixL())
	{
//		assert(_cholesky.info() != Eigen::NumericalIssue);
	}
	
	template<class URNG>
	Eigen::VectorXd operator()(URNG & rng)
	{
		Eigen::VectorXd z(_mean.rows());
		for(int64_t i = 0; i < _mean.rows(); i++) {
			z[i] = _normDist(rng);
		}
		return _mean + _cholesky.matrixL() * z;
	}
	
	double logP(Eigen::VectorXd x)
	{
		Eigen::VectorXd xDiff = x - _mean;
		double covProdDiag = _L.diagonal().prod();
		double covDet = covProdDiag * covProdDiag;
		
		return -0.5 * (
			x.rows() * log(2 * M_PI)
			+ log(covDet)
			+ xDiff.transpose() * _cholesky.solve(xDiff)
		);
	}
private:
	Eigen::VectorXd _mean;
	Eigen::LLT<Eigen::MatrixXd> _cholesky;
	Eigen::MatrixXd _L;
	std::normal_distribution<double> _normDist;
};

#endif /* defined(__netgroups__MVNormalDistribution__) */
