#ifndef __netgroups__Model__
#define __netgroups__Model__

#include "zppsim_random.hpp"

class Model
{
public:
	virtual Eigen::VectorXd parameters() = 0;
	virtual double logP() = 0;
	
	virtual Eigen::MatrixXd covarianceGuess() = 0;
	
	virtual void propose(Eigen::VectorXd const & newParams) = 0;
	virtual void accept(Eigen::VectorXd const & newParams) = 0;
	virtual void reject(Eigen::VectorXd const & oldParams) = 0;
	
	virtual void iterateDiscrete(zppsim::rng_t & rng) {}
};

#endif /* defined(__netgroups__Model__) */
